package com.ui.automation.actions;

import org.openqa.selenium.By;

/**
 * @author Douglas
 *
 */
public class ActionObjects extends ActionEngine {

	public By bSaveAndContinue =  (By.xpath("//button[contains(.,'Save and Continue')]"));
	public By hEdit = (By.linkText("Edit"));
	public By bStart = (By.xpath("//button[contains(.,'Start')]"));
	public By bSubmit = (By.xpath("//button[contains(.,'Submit')]"));
	public By bCancel = (By.xpath("//button[contains(.,'Cancel')]"));
	public By bMinimalSubmit = (By.xpath("//button[contains(.,'Minimal Submit')]"));
	public By bContinue = (By.xpath("//button[contains(.,'Continue')]"));
	public By bStartBenefitsApplication = (By.xpath("//button[contains(.,'Start Benefits Application')]"));
	public By bGoToBenefitsDiscovery = (By.xpath("//button[contains(.,'Go to Benefit Discovery')]"));
	public By bIUnderstandAndAgree =  (By.xpath("//button[contains(.,'I understand and agree')]"));
	public By bSignAndSubmit =  (By.xpath("//button[contains(.,'Sign and Submit')]"));
	public By bGetMyResults =  (By.xpath("//button[contains(.,'Get my results')]"));
	public By bMinimalSubmitLink = (By.xpath("//button[contains(.,'Submit application now with only the above information')]"));
	
	
	/**
	 * This method is used to select the Save and Continue button
	 *
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void bSaveAndContinue() throws Throwable {
		try {
			scrollPage(1000);
			scrollPageIntoView(bSaveAndContinue);
			click(bSaveAndContinue, "Save and Continue");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void bStartBenefitsApplication() throws Throwable {
		try {
			click(bStartBenefitsApplication, "Start Benefits Application");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void bGoToBenefitsDiscovery() throws Throwable {
		try {
			click(bGoToBenefitsDiscovery, "Start Benefits Discovery");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void bStart() throws Throwable {
		try {
			click(bStart, "Start");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void bMinimalSubmit() throws Throwable {
		try {
			scrollPage(1000);
			scrollPageIntoView(bMinimalSubmit);
			click(bMinimalSubmit, "Minimal Submit");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void hEdit() throws Throwable {
		try {
			click(hEdit, "Edit");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void bSubmit() throws Throwable {
		try {
			scrollPageIntoView(bSubmit);
			click(bSubmit, "Submit");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void bCancel() throws Throwable {
		try {
			scrollPageIntoView(bCancel);
			click(bCancel, "Cancel");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void bContinue() throws Throwable {
		try {
			scrollPageIntoView(bContinue);
			click(bContinue, "Continue");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void bIUnderstandAndAgree() throws Throwable {
		try {
			scrollPageIntoView(bIUnderstandAndAgree);
			click(bIUnderstandAndAgree, "I Understand and Agree");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void bSignAndSubmit() throws Throwable {
		try {
			scrollPageIntoView(bSignAndSubmit);
			click(bSignAndSubmit, "Sign and Submit");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void bGetMyResults() throws Throwable {
		try {
			scrollPageIntoView(bGetMyResults);
			click(bGetMyResults, "Get my results");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void bMinimalSubmitLink() throws Throwable {
		try {
			scrollPageIntoView(bMinimalSubmitLink);
			click(bMinimalSubmitLink, "Minimal Submit Link");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
