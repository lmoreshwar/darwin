package com.ui.automation.pages;

import java.util.HashMap;

import org.openqa.selenium.By;

import com.ui.automation.actions.ActionEngine;

/**
 * @author Douglas
 * @table ISERVE.?
 * @spreadsheet IServe_TestData.xlsx
 */
public class IserveSignaturePage extends ActionEngine {

	private By ssignature = By.name("signature"); //
	private By scheckboxSignature = By.cssSelector(".usa-checkbox__label");

	public void setData(HashMap<String, String> testData) throws Throwable {

		String dsignature = testData.get("Signature");
		String dcheckboxSignature = testData.get("CheckboxSignature");

		switch (dsignature) {
		case "null":
			break;
		case "":
			break;
		case "0":
			break;
		default:
			type(ssignature, dsignature, "Signature");
		}

		dcheckboxSignature = (String.valueOf(dcheckboxSignature).replaceFirst("\\.0+$", ""));
		switch (dcheckboxSignature) {
		case "0": // No/Disagree
			break;
		case "1": // Yes/Agree
			click(scheckboxSignature, "Signature Checkbox");
		}

		System.out.println("End of Page or Success - Signature Page.");

	}
}