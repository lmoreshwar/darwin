package com.ui.automation.pages;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;

import com.ui.automation.actions.ActionEngine;

/**
 * @author Douglas
 * @table [ISERVE].[HouseholdApplicantInfo]
 * @variable s = screen, d = data
 * @description This is a template script meant to support initial page development It contains variable formatting,
 *              structure and elements
 */
public class IserveHouseholdApplicantInfoPage extends ActionEngine {
/*
	private By sFirstName = By.id("FirstName");
	private By sMiddleName = By.id("MiddleName");
	private By sLastName = By.id("LastName");
	private By sSuffix = By.id("Suffix");
	private By sAliasName = By.id("AliasName");// [bit] NULL,
	private By sAliasName1 = By.id("AliasName1");
	private By sAliasName2 = By.id("AliasName2");
	private By sAliasName3 = By.id("AliasName3");
*/
	private By sMonth = By.name("date_month");
	private By sDay = By.name("date_day");
	private By sYear = By.name("date_year");
	String month = "(//p[contains(text(),'Household membe')]/parent::div/following::div/input[@name='date_month'])[index]";
	String day = "(//p[contains(text(),'Household membe')]/parent::div/following::div/input[@name='date_day'])[index]";
	String year = "(//p[contains(text(),'Household membe')]/parent::div/following::div/input[@name='date_year'])[index]";

	private By buttonAddHouseholdMember = By.name("Add a household member");

/*
	private By sMale = By.id("Male");// [bit] NULL,
	private By sFemale = By.id("Female");// [bit] NULL,
	private By sPreferNotToAnswer = By.id("PreferNotToAnswer");// [bit] NULL,
	private By sPhoneHome = By.id("PhoneHome");
	private By sPhoneMobile = By.id("PhoneMobile");
	private By sPhoneWork = By.id("PhoneWork");
	private By sPhoneTDDHome = By.id("PhoneTDDHome");
	private By sPhoneTDDWork = By.id("PhoneTDDWork");
	private By sPhoneOther = By.id("PhoneOther");
	private By sPhoneAdditional = By.id("PhoneOther");// [bit] NULL,
	private By sPhoneAdditionalNumber = By.id("PhoneOther");
	private By sNotYourPhoneThenWho = By.id("PhoneOther");
	private By sNotYourPhoneThenWhoVoicemails = By.id("PhoneOther");// [bit] NULL,
	private By sNoPhoneNumberAtAll = By.id("PhoneOther");// [bit] NULL,
	private By sHowToContactforApplicationInfoPostalMail = By.id("PhoneOther");// [bit] NULL,
	private By sHowToContactforApplicationInfoPostalPhone = By.id("PhoneOther");// [bit] NULL,
	private By sHowToContactforApplicationInfoPostalTextMessage = By.id("PhoneOther");// bit] NULL,
	private By sHowToContactforApplicationInfoPostalEmail = By.id("PhoneOther");// [bit] NULL,
	private By sHowToContactforApplicationInfoPostalTextMessageNumber = By.id("PhoneOther");
	private By sHowToContactforApplicationInfoPostalEmailAddress = By.id("PhoneOther");
	private By sAddEmergencyContact = By.id("PhoneOther");// [bit] NULL,
	private By sAddEmergencyContactNotNeed = By.id("PhoneOther");// [bit] NULL,
	private By sEmergencyContactFirstName = By.id("PhoneOther");
	private By sEmergencyContactMiddleName = By.id("PhoneOther");
	private By sEmergencyContactLastName = By.id("PhoneOther");
	private By sEmergencyContactSuffix = By.id("PhoneOther");
	private By sEmergencyContactPhoneNumber = By.id("PhoneOther");
	private By sEmergencyContactEmail = By.id("PhoneOther");
	private By sEmergencyContactRelationshipRoommate = By.id("PhoneOther");// [bit] NULL,
	private By sEmergencyContactRelationshipSpouse = By.id("PhoneOther");// [bit] NULL,
	private By sEmergencyContactRelationshipUnmarriedPartner = By.id("PhoneOther");// [bit] NULL,
	private By sEmergencyContactRelationshipParent = By.id("PhoneOther");// [bit] NULL,
	private By sEmergencyContactRelationshipSibling = By.id("PhoneOther");// [bit] NULL,
	private By sEmergencyContactRelationshipChild = By.id("PhoneOther");// [bit] NULL,
	private By sEmergencyContactRelationshipOtherRelative = By.id("PhoneOther");// [bit] NULL,
	private By sEmergencyContactRelationshipNoneOfTheAbove = By.id("PhoneOther");// [bit] NULL,
*/
	//private By sIntellectual = By.name("intellectual");// [bit] NULL,//Intellectual/ Development disability
	private By sIntellectual = (By.cssSelector(".usa-checkbox:nth-child(3) > .usa-checkbox__label"));

	//private By sPhysical = By.name("physical");// [bit] NULL,//Physical disability (including blindness)
	private By sPhysical = (By.cssSelector(".usa-checkbox:nth-child(4) > .usa-checkbox__label"));

	public void setData(HashMap<String, String> testData) throws Throwable {
/*
		String dFirstName = testData.get("FirstName");
		String dMiddleName = testData.get("MiddleName");
		String dLastName = testData.get("LastName");
		String dSuffix = testData.get("Suffix");
		String dAliasName = testData.get("AliasName");// [bit] NULL,
		String dAliasName1 = testData.get("AliasName1");
		String dAliasName2 = testData.get("AliasName2");
		String dAliasName3 = testData.get("AliasName3");
*/
		String dMonth = testData.get("Month");
		String dDay = testData.get("Day");
		String dYear = testData.get("Year");
/*
		String dMale = testData.get("Male");// [bit] NULL,
		String dFemale = testData.get("Female");// [bit] NULL,
		String dPreferNotToAnswer = testData.get("PreferNotToAnswer");// [bit] NULL,
		String dPhoneHome = testData.get("PhoneHome");
		String dPhoneMobile = testData.get("PhoneMobile");
		String dPhoneWork = testData.get("PhoneWork");
		String dPhoneTDDHome = testData.get("PhoneTDDHome");
		String dPhoneTDDWork = testData.get("PhoneTDDWork");
		String dPhoneOther = testData.get("PhoneOther");
		String dPhoneAdditional = testData.get("PhoneOther");// [bit] NULL,
		String dPhoneAdditionalNumber = testData.get("PhoneOther");
		String dNotYourPhoneThenWho = testData.get("PhoneOther");
		String dNotYourPhoneThenWhoVoicemails = testData.get("PhoneOther");// [bit] NULL,
		String dNoPhoneNumberAtAll = testData.get("PhoneOther");// [bit] NULL,
		String dHowToContactforApplicationInfoPostalMail = testData.get("PhoneOther");// [bit] NULL,
		String dHowToContactforApplicationInfoPostalPhone = testData.get("PhoneOther");// [bit] NULL,
		String dHowToContactforApplicationInfoPostalTextMessage = testData.get("PhoneOther");// bit] NULL,
		String dHowToContactforApplicationInfoPostalEmail = testData.get("PhoneOther");// [bit] NULL,
		String dHowToContactforApplicationInfoPostalTextMessageNumber = testData.get("PhoneOther");
		String dHowToContactforApplicationInfoPostalEmailAddress = testData.get("PhoneOther");
		String dAddEmergencyContact = testData.get("PhoneOther");// [bit] NULL,
		String dAddEmergencyContactNotNeed = testData.get("PhoneOther");// [bit] NULL,
		String dEmergencyContactFirstName = testData.get("PhoneOther");
		String dEmergencyContactMiddleName = testData.get("PhoneOther");
		String dEmergencyContactLastName = testData.get("PhoneOther");
		String dEmergencyContactSuffix = testData.get("PhoneOther");
		String dEmergencyContactPhoneNumber = testData.get("PhoneOther");
		String dEmergencyContactEmail = testData.get("PhoneOther");
		String dEmergencyContactRelationshipRoommate = testData.get("PhoneOther");// [bit] NULL,
		String dEmergencyContactRelationshipSpouse = testData.get("PhoneOther");// [bit] NULL,
		String dEmergencyContactRelationshipUnmarriedPartner = testData.get("PhoneOther");// [bit] NULL,
		String dEmergencyContactRelationshipParent = testData.get("PhoneOther");// [bit] NULL,
		String dEmergencyContactRelationshipSibling = testData.get("PhoneOther");// [bit] NULL,
		String dEmergencyContactRelationshipChild = testData.get("PhoneOther");// [bit] NULL,
		String dEmergencyContactRelationshipOtherRelative = testData.get("PhoneOther");// [bit] NULL,
		String dEmergencyContactRelationshipNoneOfTheAbove = testData.get("PhoneOther");// [bit] NULL,
*/
		String dIntellectual = testData.get("Intellectual");// [bit] NULL, //Intellectual/ Development disability
		String dPhysical = testData.get("Physical");// [bit] NULL, //Physical disability (including blindness)


		try {
/*
			switch (dFirstName) {
			case "0": // No
				break;
			case "1": // Yes
				click(sFirstName, "FirstName");
			}

			switch (dMiddleName) {
			case "0": // No
				break;
			case "1": // Yes
				click(sMiddleName, "MiddleName");
			}

			switch (dLastName) {
			case "0": // No
				break;
			case "1": // Yes
				click(sLastName, "LastName");
			}

			switch (dSuffix) {
			case "0": // No
				break;
			case "1": // Yes
				click(sSuffix, "Suffix");
			}

			switch (dAliasName) {
			case "0": // No
				break;
			case "1": // Yes
				click(sAliasName, "AliasName");
			}

			switch (dAliasName1) {
			case "0": // No
				break;
			case "1": // Yes
				click(sAliasName1, "AliasName");
			}

			switch (dAliasName2) {
			case "0": // No
				break;
			case "1": // Yes
				click(sAliasName2, "AliasName");
			}

			switch (dAliasName3) {
			case "0": // No
				break;
			case "1": // Yes
				click(sAliasName3, "AliasName");
			}
*/
			dMonth = (String.valueOf(dMonth).replaceFirst("\\.0+$", ""));
			type(sMonth, dMonth, "Month");

			dDay = (String.valueOf(dDay).replaceFirst("\\.0+$", ""));
			type(sDay, dDay, "Day");

			dYear = (String.valueOf(dYear).replaceFirst("\\.0+$", ""));
			type(sYear, dYear, "Year");

			//Intellectual/ Development disability
			dIntellectual = (String.valueOf(dIntellectual).replaceFirst("\\.0+$", ""));
			switch (dIntellectual) {
			case "0": // None, Null, Empty
				break;
			case "1": // Value is present
				click(sIntellectual, "Intellectual/ Development disability");
			}

			//Physical disability (including blindness)
			dPhysical = (String.valueOf(dPhysical).replaceFirst("\\.0+$", ""));
			switch (dPhysical) {
			case "0": // None, Null, Empty
				break;
			case "1": // Value is present
				click(sPhysical, "Physical disability (including blindness)");
				break;
			case "Yes": // Value is present
				click(sPhysical, "Physical disability (including blindness)");
			}

/*
			switch (dMale) {
			case "0": // No
				break;
			case "1": // Yes
				click(sMale, "Male");
			}

			switch (dFemale) {
			case "0": // No
				break;
			case "1": // Yes
				click(sFemale, "Female");
			}

			switch (dPreferNotToAnswer) {
			case "0": // No
				break;
			case "1": // Yes
				click(sPreferNotToAnswer, "AliasName");
			}

			switch (dPhoneHome) {
			case "0": // None, Null, Empty
				break;
			case "1": // Value is present
				click(sPhoneHome, "PhoneHome");
			}

			switch (dPhoneMobile) {
			case "0": // None, Null, Empty
				break;
			case "1": // Value is present
				click(sPhoneMobile, "PhoneMobile");
			}

			switch (dPhoneWork) {
			case "0": // No
				break;
			case "1": // Yes
				click(sPhoneWork, "PhoneWork");
			}

			switch (dPhoneTDDHome) {
			case "0": // No
				break;
			case "1": // Yes
				click(sPhoneTDDHome, "PhoneTDDHome");
			}

			switch (dPhoneTDDWork) {
			case "0": // No
				break;
			case "1": // Yes
				click(sPhoneTDDWork, "PhoneTDDWork");
			}

			switch (dPhoneOther) {
			case "0": // No
				break;
			case "1": // Yes
				click(sPhoneOther, "PhoneOther");
			}

			switch (dPhoneAdditional) {
			case "0": // No
				break;
			case "1": // Yes
				click(sPhoneAdditional, "PhoneAdditional");
			}

			switch (dPhoneAdditionalNumber) {
			case "0": // No
				break;
			case "1": // Yes
				click(sPhoneAdditionalNumber, "PhoneAdditionalNumber");
			}

			switch (dNotYourPhoneThenWho) {
			case "0": // No
				break;
			case "1": // Yes
				click(sNotYourPhoneThenWhoVoicemails, "NotYourPhoneThenWhoVoicemails");
			}

			switch (dNotYourPhoneThenWhoVoicemails) {
			case "0": // No
				break;
			case "1": // Yes
				click(sNotYourPhoneThenWhoVoicemails, "NotYourPhoneThenWhoVoicemails");
			}

			switch (dNoPhoneNumberAtAll) {
			case "0": // No
				break;
			case "1": // Yes
				click(sNoPhoneNumberAtAll, "NoPhoneNumberAtAll");
			}

			switch (dHowToContactforApplicationInfoPostalMail) {
			case "0": // No
				break;
			case "1": // Yes
				click(sHowToContactforApplicationInfoPostalMail, "HowToContactforApplicationInfoPostalMail");
			}

			switch (dHowToContactforApplicationInfoPostalPhone) {
			case "0": // No
				break;
			case "1": // Yes
				click(sHowToContactforApplicationInfoPostalPhone, "HowToContactforApplicationInfoPostalPhone");
			}

			switch (dHowToContactforApplicationInfoPostalTextMessage) {
			case "0": // No
				break;
			case "1": // Yes
				click(sHowToContactforApplicationInfoPostalTextMessage,
						"HowToContactforApplicationInfoPostalTextMessage");
			}

			switch (dHowToContactforApplicationInfoPostalEmail) {
			case "0": // No
				break;
			case "1": // Yes
				click(sHowToContactforApplicationInfoPostalEmail, "HowToContactforApplicationInfoPostalEmail");
			}

			switch (dHowToContactforApplicationInfoPostalTextMessageNumber) {
			case "0": // No
				break;
			case "1": // Yes
				click(sHowToContactforApplicationInfoPostalTextMessageNumber,
						"HowToContactforApplicationInfoPostalTextMessageNumber");
			}

			switch (dHowToContactforApplicationInfoPostalEmailAddress) {
			case "0": // No
				break;
			case "1": // Yes
				click(sHowToContactforApplicationInfoPostalEmailAddress,
						"HowToContactforApplicationInfoPostalEmailAddress");
			}

			switch (dAddEmergencyContact) {
			case "0": // No
				break;
			case "1": // Yes
				click(sAddEmergencyContact, "AddEmergencyContact");
			}

			switch (dAddEmergencyContactNotNeed) {
			case "0": // No
				break;
			case "1": // Yes
				click(sAddEmergencyContactNotNeed, "AddEmergencyContactNotNeed");
			}

			switch (dEmergencyContactFirstName) {
			case "0": // No
				break;
			case "1": // Yes
				click(sEmergencyContactFirstName, "EmergencyContactFirstName");
			}

			switch (dEmergencyContactMiddleName) {
			case "0": // No
				break;
			case "1": // Yes
				click(sEmergencyContactMiddleName, "EmergencyContactMiddleName");
			}

			switch (dEmergencyContactLastName) {
			case "0": // No
				break;
			case "1": // Yes
				click(sEmergencyContactLastName, "EmergencyContactLastName");
			}

			switch (dEmergencyContactSuffix) {
			case "0": // No
				break;
			case "1": // Yes
				click(sEmergencyContactSuffix, "EmergencyContactSuffix");
			}

			switch (dEmergencyContactPhoneNumber) {
			case "0": // No
				break;
			case "1": // Yes
				click(sEmergencyContactPhoneNumber, "EmergencyContactPhoneNumber");
			}

			switch (dEmergencyContactEmail) {
			case "0": // No
				break;
			case "1": // Yes
				click(sEmergencyContactEmail, "EmergencyContactEmail");
			}

			switch (dEmergencyContactRelationshipRoommate) {
			case "0": // No
				break;
			case "1": // Yes
				click(sEmergencyContactRelationshipRoommate, "EmergencyContactRelationshipRoommate");
			}

			switch (dEmergencyContactRelationshipSpouse) {
			case "0": // No
				break;
			case "1": // Yes
				click(sEmergencyContactRelationshipSpouse, "EmergencyContactRelationshipSpouse");
			}

			switch (dEmergencyContactRelationshipUnmarriedPartner) {
			case "0": // No
				break;
			case "1": // Yes
				click(sEmergencyContactRelationshipUnmarriedPartner, "EmergencyContactRelationshipUnmarriedPartner");
			}

			switch (dEmergencyContactRelationshipParent) {
			case "0": // No
				break;
			case "1": // Yes
				click(sEmergencyContactRelationshipParent, "EmergencyContactRelationshipParent");
			}

			switch (dEmergencyContactRelationshipSibling) {
			case "0": // No
				break;
			case "1": // Yes
				click(sEmergencyContactRelationshipSibling, "EmergencyContactRelationshipSibling");
			}

			switch (dEmergencyContactRelationshipChild) {
			case "0": // No
				break;
			case "1": // Yes
				click(sEmergencyContactRelationshipChild, "EmergencyContactRelationshipChild");
			}

			switch (dEmergencyContactRelationshipOtherRelative) {
			case "0": // No
				break;
			case "1": // Yes
				click(sEmergencyContactRelationshipOtherRelative, "EmergencyContactRelationshipOtherRelative");
			}

			switch (dEmergencyContactRelationshipNoneOfTheAbove) {
			case "0": // No
				break;
			case "1": // Yes
				click(sEmergencyContactRelationshipNoneOfTheAbove, "EmergencyContactRelationshipNoneOfTheAbove");
			}
*/
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("End of Page or Success - Household Applicant Info Page.");
	}

	public void setHouseHoledData(HashMap<String, String> testData) throws Throwable {

		String dMonth = testData.get("Month");
		String dDay = testData.get("Day");
		String dYear = testData.get("Year");
		List<String> listYears = Arrays.asList(testData.get("Year").split("_"));
		String dDisability = testData.get("Disability");
		List<String> listDisability = Arrays.asList(testData.get("Disability").split("_"));
		int disabilityIndex = 0;

		try {
			for (String yr : listYears) {
				switch (disabilityIndex) {
				case 0:
				{
				dMonth = (String.valueOf(dMonth).replaceFirst("\\.0+$", ""));
				type(sMonth, dMonth, "Month");

				dDay = (String.valueOf(dDay).replaceFirst("\\.0+$", ""));
				type(sDay, dDay, "Day");

				yr = (String.valueOf(yr).replaceFirst("\\.0+$", ""));
				type(sYear, yr, "Year");

				String disability = (String.valueOf(listDisability.get(disabilityIndex++)).replaceFirst("\\.0+$", ""));
				switch (disability) {
				case "No": // None, Null, Empty
					break;
				case "Yes": // Value is present
					click(sPhysical, "Physical disability (including blindness)");
					break;
				}
				} break;
				default:{
					click(buttonAddHouseholdMember, "+ Add a household member button");

					dMonth = (String.valueOf(dMonth).replaceFirst("\\.0+$", ""));
					type(By.xpath(month.replace("index", Integer.toString(disabilityIndex))), dMonth, "Month");

					dDay = (String.valueOf(dDay).replaceFirst("\\.0+$", ""));
					type(By.xpath(day.replace("index", Integer.toString(disabilityIndex))), dDay, "Day");

					yr = (String.valueOf(yr).replaceFirst("\\.0+$", ""));
					type(By.xpath(year.replace("index", Integer.toString(disabilityIndex))), yr, "Year");

					String disability = (String.valueOf(listDisability.get(disabilityIndex++)).replaceFirst("\\.0+$", ""));
					switch (disability) {
					case "No": // None, Null, Empty
						break;
					case "Yes": // Value is present
						click(sPhysical, "Physical disability (including blindness)");
						break;
					}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("End of Page or Success - Household Applicant Info Page.");

	}
}