package com.ui.automation.pages;

import java.util.HashMap;

import org.openqa.selenium.By;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;

/**
 * @author Douglas
 * @table ISERVE.?
 * @spreadsheet IServe_TestData.xlsx
 */
public class IserveAuthorizedRepresentativePage extends ActionEngine {

	private By sAuthorizedRepYes = By.xpath("//*[@name='authorizedRepCheckBox' and @value='Yes']/following-sibling::label");
	private By sAuthorizedRepNo = By.xpath("//*[@name='authorizedRepCheckBox' and @value='No']/following-sibling::label");

	public void setData(HashMap<String, String> testData) throws Throwable {

		ActionObjects ActionObjects = new ActionObjects();
		String dAuthorizedRep = testData.get("AuthorizedRep");

		dAuthorizedRep = (String.valueOf(dAuthorizedRep).replaceFirst("\\.0+$", ""));
		switch (dAuthorizedRep) {
		case "0": // No/Disagree
			click(sAuthorizedRepNo, "Authorized Representative No");
			break;
		case "1": // Yes/Agree
			click(sAuthorizedRepYes, "Authorized Representative Yes");
		}

		ActionObjects.bSaveAndContinue();
		System.out.println("End of Page or Success - You can appoint an authorized representative.");

	}
}