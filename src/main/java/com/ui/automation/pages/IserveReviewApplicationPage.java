package com.ui.automation.pages;

import java.util.HashMap;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;

/**
 * @author Douglas
 * @table ISERVE.?
 *
 */
public class IserveReviewApplicationPage extends ActionEngine {

//	private By sFirstName = By.name("firstName");
//	private By sMiddleName = By.name("middleName");
//	private By sLastName = By.name("lastName");
//	private By sSuffix = By.name("suffix");

	public void setData(HashMap<String, String> testData) throws Throwable {
		ActionObjects actionobjects = new ActionObjects();
		ActionEngine actionengine = new ActionEngine();

//		String dFirstName = testData.get("FirstName");
//		String dMiddleName = testData.get("MiddleName");
//		String dLastName = testData.get("LastName");
//		String dSuffix = testData.get("Suffix");
		
		String dContinueYourApplication = testData.get("ContinueYourApplication");
		String dSubmitApplicationNow = testData.get("SubmitApplicationNow");

		dContinueYourApplication = (String.valueOf(dContinueYourApplication).replaceFirst("\\.0+$", ""));
		if (dContinueYourApplication.equals("1") ) {
			
			switch (dContinueYourApplication) {
			case "null":
				break;
			case "":
				break;
			case "0":
				break;
			case "1":
				actionobjects.bContinue();
			}
		}
		
		dSubmitApplicationNow = (String.valueOf(dSubmitApplicationNow).replaceFirst("\\.0+$", ""));
		if (dSubmitApplicationNow.equals("1") ) {
		
			switch (dSubmitApplicationNow) {
			case "null":
				break;
			case "":
				break;
			case "0":
				break;
			case "1":
				actionobjects.bMinimalSubmitLink();
			}
		}
		System.out.println("End of Page or Success - Review and Confirm: Make sure everything looks correct");
	}
}