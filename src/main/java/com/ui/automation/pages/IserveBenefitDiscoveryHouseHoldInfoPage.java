package com.ui.automation.pages;

import java.util.HashMap;

import org.openqa.selenium.By;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;

/**
 * @author Douglas
 * @table ISERVE.?
 * @spreadsheet IServe_TestData.xlsx
 */
public class IserveBenefitDiscoveryHouseHoldInfoPage extends ActionEngine {

	ActionObjects actionobjects = new ActionObjects();
	private By sStayInHouseYes = By.xpath("//*[@name='service' and @value='Yes']/following-sibling::label");
	private By sStayInHouseNo = By.xpath("//*[@name='service' and @value='No']/following-sibling::label");
	private By sPregnantYes = By.cssSelector(".benefit-discovery-margin-bottom:nth-child(3) div:nth-child(2) .usa-radio__label");
	private By sPregnantNo = By.cssSelector(".benefit-discovery-margin-bottom:nth-child(3) div:nth-child(3) .usa-radio__label");
	private By sHouseholdIncome = By.name("houseHoldIcome");

	public void setData(HashMap<String, String> testData) throws Throwable {

		String dStayInHouse = testData.get("StayInHouse");
		String dHouseholdIncome = testData.get("HouseholdIncome");
		String dPregnant = testData.get("Pregnant");

		try {

			dPregnant = (String.valueOf(dPregnant).replaceFirst("\\.0+$", ""));
			switch (dPregnant) {
			case "0": // No
				click(sPregnantNo, "Do you need services to stay in your home = No");
				break;
			case "1": // Yes
				click(sPregnantYes, "Do you need services to stay in your home = Yes");
			case "No": // No
				click(sPregnantNo, "Do you need services to stay in your home = No");
				break;
			case "Yes": // Yes
				click(sPregnantYes, "Do you need services to stay in your home = Yes");
			}

			dStayInHouse = (String.valueOf(dStayInHouse).replaceFirst("\\.0+$", ""));
			switch (dStayInHouse) {
			case "0": // No
				click(sStayInHouseNo, "Do you need services to stay in your home = No");
				break;
			case "1": // Yes
				click(sStayInHouseYes, "Do you need services to stay in your home = Yes");
			case "No": // No
				click(sStayInHouseNo, "Do you need services to stay in your home = No");
				break;
			case "Yes": // Yes
				click(sStayInHouseYes, "Do you need services to stay in your home = Yes");
			}

			dHouseholdIncome = (String.valueOf(dHouseholdIncome).replaceFirst("\\.0+$", ""));
			switch (dHouseholdIncome) {
			case "null":
				break;
			case "":
				break;
			case "0":
				break;
			default:
				type(sHouseholdIncome, dHouseholdIncome, "Household Income: " + dHouseholdIncome);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		waitForSeconds(1);
		actionobjects.bGetMyResults();
		System.out.println("End of Page or Success - Household Page.");

	}

}