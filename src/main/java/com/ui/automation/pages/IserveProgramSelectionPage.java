package com.ui.automation.pages;

import java.util.HashMap;

import org.openqa.selenium.By;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;

/**
 * @author Douglas
 * @table ISERVE.ProgramSelection
 * @spreadsheet IServe_TestData.xlsx
 */
public class IserveProgramSelectionPage extends ActionEngine {

	ActionObjects actionobjects = new ActionObjects();
	private By sMedicaid = By.cssSelector(".usa-form-group:nth-child(1) > .usa-checkbox__label");
	private By sHCBS = By.cssSelector(".usa-form-group:nth-child(2) > .usa-checkbox__label");
	private By sSNAP = By.cssSelector(".usa-form-group:nth-child(3) > .usa-checkbox__label");
	private By sLIHEAP = By.cssSelector(".usa-form-group:nth-child(4) > .usa-checkbox__label");
	private By sRRP = By.cssSelector(".usa-form-group:nth-child(5) > .usa-checkbox__label");
	private By sADC = By.cssSelector(".usa-form-group:nth-child(6) > .usa-checkbox__label");
	private By sCCS = By.cssSelector(".usa-form-group:nth-child(7) > .usa-checkbox__label");
	private By sEMA = By.cssSelector(".usa-form-group:nth-child(8) > .usa-checkbox__label");
	private By sAABD = By.cssSelector(".usa-form-group:nth-child(9) > .usa-checkbox__label");
	private By sSDP = By.cssSelector(".usa-form-group:nth-child(10) > .usa-checkbox__label");
	private By sSSAD = By.cssSelector(".usa-form-group:nth-child(11) > .usa-checkbox__label");
	private By sPAS = By.cssSelector(".usa-form-group:nth-child(12) > .usa-checkbox__label");
	private By sSSAD1 = By.cssSelector(".usa-fieldset:nth-child(1) .usa-checkbox:nth-child(1) > .usa-checkbox__label");
	private By sSSAD2 = By.cssSelector(".usa-fieldset:nth-child(1) .usa-checkbox:nth-child(2) > .usa-checkbox__label");
	private By sSSAD3 = By.cssSelector(".usa-fieldset:nth-child(1) .usa-checkbox:nth-child(3) > .usa-checkbox__label");
	private By sSSAD4 = By.cssSelector(".usa-fieldset:nth-child(1) .usa-checkbox:nth-child(4) > .usa-checkbox__label");
	private By sSSAD5 = By.cssSelector(".usa-fieldset:nth-child(1) .usa-checkbox:nth-child(5) > .usa-checkbox__label");
	private By sSSAD6 = By.cssSelector(".usa-fieldset:nth-child(1) .usa-checkbox:nth-child(6) > .usa-checkbox__label");

	public void setData(HashMap<String, String> testData) throws Throwable {

		String dMedicaid = testData.get("Medicaid");
		String dHCBS = testData.get("HCBS");
		String dSNAP = testData.get("SNAP");
		String dLIHEAP = testData.get("LIHEAP");
		String dRRP = testData.get("RRP");
		String dADC = testData.get("ADC");
		String dCCS = testData.get("CCS");
		String dEMA = testData.get("EMA");
		String dAABD = testData.get("AABD");
		String dSDP = testData.get("SDP");
		String dSSAD = testData.get("SSAD");
		String dPAS = testData.get("PAS");

		try {

			dMedicaid = (String.valueOf(dMedicaid).replaceFirst("\\.0+$", ""));
			switch (dMedicaid) {
			case "0": // No
				break;
			case "1": // Yes
				click(sMedicaid, "Medicaid");
			}

			dHCBS = (String.valueOf(dHCBS).replaceFirst("\\.0+$", ""));
			switch (dHCBS) {
			case "0": // No
				break;
			case "1": // Yes
				click(sHCBS, "HCBS");
			}

			dSNAP = (String.valueOf(dSNAP).replaceFirst("\\.0+$", ""));
			switch (dSNAP) {
			case "0": // No
				break;
			case "1": // Yes
				click(sSNAP, "SNAP");
			}

			dLIHEAP = (String.valueOf(dLIHEAP).replaceFirst("\\.0+$", ""));
			switch (dLIHEAP) {
			case "0": // No
				break;
			case "1": // Yes
				click(sLIHEAP, "LIHEAP");
			}

			dRRP = (String.valueOf(dRRP).replaceFirst("\\.0+$", ""));
			switch (dRRP) {
			case "0": // No
				break;
			case "1": // Yes
				click(sRRP, "RRP");
			}

			dADC = (String.valueOf(dADC).replaceFirst("\\.0+$", ""));
			switch (dADC) {
			case "0": // No
				break;
			case "1": // Yes
				click(sADC, "ADC");
			}

			dCCS = (String.valueOf(dCCS).replaceFirst("\\.0+$", ""));
			switch (dCCS) {
			case "0": // No
				break;
			case "1": // Yes
				click(sCCS, "CCS");
			}

			dEMA = (String.valueOf(dEMA).replaceFirst("\\.0+$", ""));
			switch (dEMA) {
			case "0": // No
				break;
			case "1": // Yes
				click(sEMA, "EMA");
			}

			dAABD = (String.valueOf(dAABD).replaceFirst("\\.0+$", ""));
			switch (dAABD) {
			case "0": // No
				break;
			case "1": // Yes
				click(sAABD, "AABD");
			}

			dSDP = (String.valueOf(dSDP).replaceFirst("\\.0+$", ""));
			switch (dSDP) {
			case "0": // No
				break;
			case "1": // Yes
				click(sSDP, "SDP");
			}

			dSSAD = (String.valueOf(dSSAD).replaceFirst("\\.0+$", ""));
			switch (dSSAD) {
			case "0": // No
				break;
			case "1": // Yes
				click(sSSAD, "SSAD");

				String dSSAD1 = testData.get("SSAD1");

				dSSAD1 = (String.valueOf(dSSAD1).replaceFirst("\\.0+$", ""));
				switch (dSSAD1) {
				case "0": // No
					break;
				case "1": // Yes
					click(sSSAD1, "SSAD1");
				}

				String dSSAD2 = testData.get("SSAD2");

				dSSAD2 = (String.valueOf(dSSAD2).replaceFirst("\\.0+$", ""));
				switch (dSSAD2) {
				case "0": // No
					break;
				case "1": // Yes
					click(sSSAD2, "SSAD2");
				}

				String dSSAD3 = testData.get("SSAD3");

				dSSAD3 = (String.valueOf(dSSAD3).replaceFirst("\\.0+$", ""));
				switch (dSSAD3) {
				case "0": // No
					break;
				case "1": // Yes
					click(sSSAD3, "SSAD3");
				}

				String dSSAD4 = testData.get("SSAD4");

				dSSAD4 = (String.valueOf(dSSAD4).replaceFirst("\\.0+$", ""));
				switch (dSSAD4) {
				case "0": // No
					break;
				case "1": // Yes
					click(sSSAD4, "SSAD4");
				}

				String dSSAD5 = testData.get("SSAD5");

				dSSAD5 = (String.valueOf(dSSAD5).replaceFirst("\\.0+$", ""));
				switch (dSSAD5) {
				case "0": // No
					break;
				case "1": // Yes
					click(sSSAD5, "SSAD5");
				}

				String dSSAD6 = testData.get("SSAD6");

				dSSAD6 = (String.valueOf(dSSAD6).replaceFirst("\\.0+$", ""));
				switch (dSSAD6) {
				case "0": // No
					break;
				case "1": // Yes
					click(sSSAD6, "SSAD6");
				}

			}

			dPAS = (String.valueOf(dPAS).replaceFirst("\\.0+$", ""));
			switch (dPAS) {
			case "0": // No
				break;
			case "1": // Yes
				click(sPAS, "PAS");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	actionobjects.bSaveAndContinue();
	System.out.println("End of Page or Success - Program Selection Page - Please select one or more programs below.");
	}

}
