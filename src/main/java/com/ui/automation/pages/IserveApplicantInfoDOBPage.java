package com.ui.automation.pages;

import java.util.HashMap;

import org.openqa.selenium.By;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;

/**
 * @author Douglas
 * @table [ISERVE].[HouseholdApplicantInfo]
 *
 */
public class IserveApplicantInfoDOBPage extends ActionEngine {

	private By sMonth = By.name("date_month");
	private By sDay = By.name("date_day");
	private By sYear = By.name("date_year");
	private By sMale = By.xpath("//*[@name='Sex' and @value='Male']/following-sibling::label");
	private By sFemale = By.xpath("//*[@name='Sex' and @value='Female']/following-sibling::label");

	public void setData(HashMap<String, String> testData) throws Throwable {

		ActionObjects ActionObjects = new ActionObjects();
		String dMonth = testData.get("Month");
		String dDay = testData.get("Day");
		String dYear = testData.get("Year");
		String dSex = testData.get("Sex");

		switch (dMonth) {
		case "null":
			break;
		case "":
			break;
		case "0":
			break;
		default:
			type(sMonth, dMonth, "Month");
		}

		switch (dDay) {
		case "null":
			break;
		case "":
			break;
		case "0":
			break;
		default:
			type(sDay, dDay, "Day");
		}

		switch (dYear) {
		case "null":
			break;
		case "":
			break;
		case "0":
			break;
		default:
			type(sYear, dYear, "Year");
		}

		switch (dSex) {
		case "Male":
			click(sMale, "Male");
			break;
		case "Female":
			click(sFemale, "Female");
			break;
		case "null":
			break;
		case "":
			break;
		case "0":
			break;
		}

		ActionObjects.bSaveAndContinue();
		System.out.println("End of Page or Success - Household Applicant Info Page.");
	}
}