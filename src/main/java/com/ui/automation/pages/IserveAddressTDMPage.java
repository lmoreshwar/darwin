package com.ui.automation.pages;

import java.util.HashMap;
//import java.util.Hashtable;

import org.openqa.selenium.By;

import com.ui.automation.actions.ActionEngine;

/**
 * @author Douglas
 * @table ISERVE.Applicant
 */
public class IserveAddressTDMPage extends ActionEngine {

	private By sAdd1 = By.name("homeAddress.line_1");
	private By sAdd2 = By.name("homeAddress.line_2");
	private By sCity = By.name("homeAddress.city");
	private By sCounty = By.name("homeAddress.county");
	private By sState = By.name("homeAddress.state");
	private By sPostalCode5 = By.name("homeAddress.zip5");
	private By sPostalCode4 = By.name("homeAddress.zip4");
	private By sMailingAddress = By.cssSelector(".usa-form-group:nth-child(1) > .usa-radio__label");

	public void setDBData(HashMap<String, String> testData) throws Throwable {

		String dAdd1 = testData.get("add1");
		String dAdd2 = testData.get("add2");
		// String add3 = testData.get("add3");
		String dCity = testData.get("city");
		String dCounty = testData.get("county");
		String dState = testData.get("state");
		String dPostalCode5 = testData.get("postalCode5");
		dPostalCode5 = (String.valueOf(dPostalCode5).replaceFirst("\\.0+$", ""));
		String dPostalCode4 = testData.get("postalCode4");
		dPostalCode4 = (String.valueOf(dPostalCode4).replaceFirst("\\.0+$", ""));

		type(sAdd1, dAdd1, "Street Address");
		type(sAdd2, dAdd2, "Street Address 2 (Optional)");
		type(sCity, dCity, "City");
		selectByValue(sCounty, dCounty, "County");
		selectByValue(sState, dState, "State");
		type(sPostalCode5, dPostalCode5, "PostalCode5");
		type(sPostalCode4, dPostalCode4, "PostalCode4");

		click(sMailingAddress, "Is this your mailing adress = Yes");

		System.out.println(
				"End of Page or Success - Applicant Address Page - Tell us about your home address Legally as it appears on your Social Security Card.");

	}

}

