package com.ui.automation.pages;

import java.util.HashMap;

import org.openqa.selenium.By;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;

/**
 * @author Douglas
 * @table ISERVE.?
 * @spreadsheet IServe_TestData.xlsx
 */
public class IserveLegalQuestionsPage extends ActionEngine {

	ActionObjects actionobjects = new ActionObjects();
	private By sLegalQuestion1Agree = By.cssSelector(".usa-fieldset:nth-child(2) .usa-radio:nth-child(1) > .usa-radio__label");
	private By sLegalQuestion1Disagree = By.cssSelector(".usa-fieldset:nth-child(2) .usa-radio:nth-child(2) > .usa-radio__label");

	private By sLegalQuestion2Agree = By.cssSelector(".usa-fieldset:nth-child(3) .usa-radio:nth-child(1) > .usa-radio__label");
	private By sLegalQuestion2Disagree = By.cssSelector(".usa-fieldset:nth-child(3) .usa-radio:nth-child(2) > .usa-radio__label");

	private By sLegalQuestion3Agree = By.cssSelector(".usa-fieldset:nth-child(4) .usa-radio:nth-child(1) > .usa-radio__label");
	private By sLegalQuestion3Disagree = By.cssSelector(".usa-fieldset:nth-child(4) .usa-radio:nth-child(2) > .usa-radio__label");

	private By sLegalQuestion4Agree = By.cssSelector(".usa-fieldset:nth-child(5) .usa-radio:nth-child(1) > .usa-radio__label");
	private By sLegalQuestion4Disagree = By.cssSelector(".usa-fieldset:nth-child(5) .usa-radio:nth-child(2) > .usa-radio__label");

	private By sLegalQuestion5Agree = By.cssSelector(".usa-fieldset:nth-child(6) .usa-radio:nth-child(1) > .usa-radio__label");
	private By sLegalQuestion5Disagree = By.cssSelector(".usa-fieldset:nth-child(6) .usa-radio:nth-child(2) > .usa-radio__label");

	private By sLegalQuestion6Agree = By.cssSelector(".usa-fieldset:nth-child(7) .usa-radio:nth-child(1) > .usa-radio__label");
	private By sLegalQuestion6Disagree = By.cssSelector(".usa-fieldset:nth-child(7) .usa-radio:nth-child(2) > .usa-radio__label");

	public void setData(HashMap<String, String> testData) throws Throwable {

		String dLegalQuestion1 = testData.get("LegalQuestion1");
		String dLegalQuestion2 = testData.get("LegalQuestion2");
		String dLegalQuestion3 = testData.get("LegalQuestion3");
		String dLegalQuestion4 = testData.get("LegalQuestion4");
		String dLegalQuestion5 = testData.get("LegalQuestion5");
		String dLegalQuestion6 = testData.get("LegalQuestion6");

		try {

			dLegalQuestion1 = (String.valueOf(dLegalQuestion1).replaceFirst("\\.0+$", ""));
			switch (dLegalQuestion1) {
			case "0":
				click(sLegalQuestion1Disagree, "Disagree - ");
				break;
			case "1":
				click(sLegalQuestion1Agree, "Agree - ");
			}

			dLegalQuestion2 = (String.valueOf(dLegalQuestion2).replaceFirst("\\.0+$", ""));
			switch (dLegalQuestion2) {
			case "0":
				click(sLegalQuestion2Disagree, "Disagree - ");
				break;
			case "1":
				click(sLegalQuestion2Agree, "Agree - ");
			}

			dLegalQuestion3 = (String.valueOf(dLegalQuestion3).replaceFirst("\\.0+$", ""));
			switch (dLegalQuestion3) {
			case "0":
				click(sLegalQuestion3Disagree, "Disagree - ");
				break;
			case "1":
				click(sLegalQuestion3Agree, "Agree - ");
			}

			dLegalQuestion4 = (String.valueOf(dLegalQuestion4).replaceFirst("\\.0+$", ""));
			switch (dLegalQuestion4) {
			case "0":
				click(sLegalQuestion4Disagree, "Disagree - ");
				break;
			case "1":
				click(sLegalQuestion4Agree, "Agree - ");
			}

			dLegalQuestion5 = (String.valueOf(dLegalQuestion5).replaceFirst("\\.0+$", ""));
			switch (dLegalQuestion5) {
			case "0":
				click(sLegalQuestion5Disagree, "Disagree - ");
				break;
			case "1":
				click(sLegalQuestion5Agree, "Agree - ");
			}

			dLegalQuestion6 = (String.valueOf(dLegalQuestion6).replaceFirst("\\.0+$", ""));
			switch (dLegalQuestion6) {
			case "0":
				click(sLegalQuestion6Disagree, "Disagree - ");
				break;
			case "1":
				click(sLegalQuestion6Agree, "Agree - ");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		actionobjects.bSaveAndContinue();
		System.out.println(
				"End of Page or Success - Legal Questions Page - Document Upload Application Submission End - Next, read the following statements and indicate whether you agree or disagree with each statement.");

	}

}