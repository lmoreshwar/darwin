package com.ui.automation.pages;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.HashMap;

import org.openqa.selenium.By;
//import org.openqa.selenium.JavascriptExecutor;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;

/**
 * @author Douglas
 *
 */
public class IserveProgramSelectPrivacyPage extends ActionEngine {

	ActionObjects actionobjects = new ActionObjects();
	private By sScroll1 = By.cssSelector(".usa-card__body > div");
	private By sScroll2 = By.xpath("//u[normalize-space()='CONTACT INFORMATION']");

	public void setData(HashMap<String, String> testData) throws Throwable {

		scrollPage(1000);
		click(sScroll1, "Notice of Information Privacy Practices");
		click(sScroll2, "Notice of Information Privacy Practices");

		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_END);
		robot.keyRelease(KeyEvent.VK_END);
		robot.keyRelease(KeyEvent.VK_CONTROL);

		actionobjects.bIUnderstandAndAgree();
		System.out.println("End of Page or Success - Notice of Information Privacy Practices Page.");

	}
}