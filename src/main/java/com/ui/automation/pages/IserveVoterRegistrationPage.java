package com.ui.automation.pages;

import java.util.HashMap;

import org.openqa.selenium.By;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;

/**
 * @author Douglas
 * @table ISERVE.?
 * @spreadsheet IServe_TestData.xlsx
 */
public class IserveVoterRegistrationPage extends ActionEngine {

	ActionObjects actionobjects = new ActionObjects();
	private By sVoterRegistrationYes = By.xpath("//label[contains(text(),'Yes')]");
	private By sVoterRegistrationNo = By.xpath("//label[contains(text(),'No')]");

	public void setData(HashMap<String, String> testData) throws Throwable {

		String dVoterRegistration = testData.get("VoterRegistration");

		try {

			dVoterRegistration = (String.valueOf(dVoterRegistration).replaceFirst("\\.0+$", ""));
			switch (dVoterRegistration) {
			case "0": // No
				click(sVoterRegistrationYes, "VoterRegistration Yes");
				break;
			case "1": // Yes
				click(sVoterRegistrationNo, "VoterRegistration No");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		actionobjects.bSaveAndContinue();
		System.out.println("End of Page or Success - Voter Registration Page");
	}

}