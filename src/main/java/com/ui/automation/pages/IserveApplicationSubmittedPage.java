package com.ui.automation.pages;

import java.util.HashMap;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;

/**
 * @author Douglas
 * @table ISERVE.?
 *
 */
public class IserveApplicationSubmittedPage extends ActionEngine {


	public void setData(HashMap<String, String> testData) throws Throwable {

		ActionObjects actionobjects = new ActionObjects();

		actionobjects.bSignAndSubmit();
		System.out.println("End of Page or Success - Application Submitted Page - Congratulations! Your application has been successfully submitted page");

	}
}