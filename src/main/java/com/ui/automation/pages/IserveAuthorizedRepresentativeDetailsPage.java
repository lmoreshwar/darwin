package com.ui.automation.pages;

import java.util.HashMap;

import org.openqa.selenium.By;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;

/**
 * @author Douglas
 * @table ISERVE.?
 * @spreadsheet IServe_TestData.xlsx
 */
public class IserveAuthorizedRepresentativeDetailsPage extends ActionEngine {

	private By sFirstName = By.name("firstName");
	private By sMiddleName = By.name("middleName");
	private By sLastName = By.name("lastName");
	private By sSuffix = By.name("suffix");
	private By sGuardian = By.xpath("//label[contains(text(),'Guardian')]");
	private By sConservator = By.xpath("//label[contains(text(),'Conservator')]");
	private By sPowerOfAttorney = By.xpath("//label[contains(text(),'Power of Attorney')]");
	private By sAuthorizedRepresentative = By.xpath("//label[contains(text(),'Authorized Representative')]");

	public void setData(HashMap<String, String> testData) throws Throwable {

		ActionObjects actionobjects = new ActionObjects();
		String dFirstName = testData.get("AuthorizedRepFirstName");
		String dMiddleName = testData.get("AuthorizedRepMiddleName");
		String dLastName = testData.get("AuthorizedRepLastName");
		String dSuffix = testData.get("AuthorizedRepSuffix");
		String dGuardian = testData.get("Guardian");
		String dConservator = testData.get("Conservator");
		String dPowerOfAttorney = testData.get("PowerOfAttorney");
		String dAuthorizedRepresentative = testData.get("AuthorizedRepresentative");

		try {

			switch (dFirstName) {
			case "null":
				break;
			case "":
				break;
			case "0":
				break;
			default:
				type(sFirstName, dFirstName, "First name");
			}

			switch (dMiddleName) {
			case "null":
				break;
			case "":
				break;
			case "0":
				break;
			default:
				type(sMiddleName, dMiddleName, "Middle name");
			}

			switch (dLastName) {
			case "null":
				break;
			case "":
				break;
			case "0":
				break;
			default:
				type(sLastName, dLastName, "Last name");
			}

			switch (dSuffix) {
			case "null":
				break;
			case "":
				break;
			case "0":
				break;
			default:
				type(sSuffix, dSuffix, "Suffix");
			}

			dGuardian = (String.valueOf(dGuardian).replaceFirst("\\.0+$", ""));
			switch (dGuardian) {
			case "0": // No
				break;
			case "1": // Yes
				click(sGuardian, "Stable address - I  have a stable place to live");
			}
			dConservator = (String.valueOf(dConservator).replaceFirst("\\.0+$", ""));
			switch (dConservator) {
			case "0": // No
				break;
			case "1": // Yes
				click(sConservator, "Temporary address - I have a place or places to live, but they aren't permanent");
			}
			dPowerOfAttorney = (String.valueOf(dPowerOfAttorney).replaceFirst("\\.0+$", ""));
			switch (dPowerOfAttorney) {
			case "0": // No
				break;
			case "1": // Yes
				click(sPowerOfAttorney, "Homeless - I don't currently have an address");
			}
			dAuthorizedRepresentative = (String.valueOf(dAuthorizedRepresentative).replaceFirst("\\.0+$", ""));
			switch (dAuthorizedRepresentative) {
			case "0": // No
				break;
			case "1": // Yes
				click(sAuthorizedRepresentative, "Homeless - I don't currently have an address");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		actionobjects.bSaveAndContinue();
		System.out.println("End of Page or Success - Autorized Representative Details Page");
	}
}
