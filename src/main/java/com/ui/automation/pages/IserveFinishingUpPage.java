package com.ui.automation.pages;

import java.util.HashMap;

import org.openqa.selenium.By;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;

/**
 * @author Douglas
 * @table ISERVE.?
 * @spreadsheet IServe_TestData.xlsx
 */
public class IserveFinishingUpPage extends ActionEngine {

	ActionObjects actionobjects = new ActionObjects();
	private By sHelpfulDetails = By.xpath("//textarea[@name='additionalComments']");

	public void setData(HashMap<String, String> testData) throws Throwable {

		String dHelpfulDetails = testData.get("HelpfulDetails");

		dHelpfulDetails = (String.valueOf(dHelpfulDetails).replaceFirst("\\.0+$", ""));
		switch (dHelpfulDetails) {
		case "null":
			break;
		case "":
			break;
		case "0":
			break;
		default:
			type(sHelpfulDetails, dHelpfulDetails, "Finishing Up helpful details");
		}

		actionobjects.bSaveAndContinue();
		System.out.println("End of Page or Success - Finishing Up Page.");

	}
}