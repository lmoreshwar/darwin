package com.ui.automation.pages;

import java.util.HashMap;

import org.openqa.selenium.By;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;

/**
 * @author Douglas
 * @table ISERVE.?
 * @spreadsheet IServe_TestData.xlsx
 */
public class IserveHouseholdAddressPage extends ActionEngine {

	private By sAdd1 = By.name("homeAddress.line_1");
	private By sAdd2 = By.name("homeAddress.line_2");
	private By sCity = By.name("homeAddress.city");
	private By sCounty = By.name("homeAddress.county");
	private By sState = By.name("homeAddress.state");
	private By sPostalCode5 = By.name("homeAddress.zip5");
	private By sPostalCode4 = By.name("homeAddress.zip4");
	private By sInCareOfYes = By.xpath("//*[@name='inCareOfCheckbox' and @value='true']/following-sibling::label");
	private By sInCareOfNo = By.xpath("//*[@name='inCareOfCheckbox' and @value='false']/following-sibling::label");
	private By sMailingAddressYes = By.xpath("//*[@name='mailingAddressCheckBox' and @value='Y']/following-sibling::label");
	private By sMailingAddressNo = By.xpath("//*[@name='mailingAddressCheckBox' and @value='N']/following-sibling::label");
	private By sInCareOfName = By.name("inCareOfName");

	private By sHouse = By.cssSelector(".usa-form-group:nth-child(1) > .usa-radio__label");
	private By sRoomAndBoard = By.cssSelector(".usa-form-group:nth-child(2) > .usa-radio__label");
	private By sNursingHome = By.cssSelector(".usa-form-group:nth-child(3) > .usa-radio__label");
	private By sCenterDevDisabled = By.cssSelector(".usa-form-group:nth-child(4) > .usa-radio__label");
	private By sApartment = By.cssSelector(".usa-form-group:nth-child(5) > .usa-radio__label");
	private By sGroupHome = By.cssSelector(".usa-form-group:nth-child(6) > .usa-radio__label");
	private By sOther = By.cssSelector(".usa-form-group:nth-child(7) > .usa-radio__label");
	//private By sOtherLivingInformation = By.cssSelector("Specify Other Living Arrangements");
	private By sOtherLivingInformation = By.name("otherLivingInformation");
	
	public void setData(HashMap<String, String> testData) throws Throwable {

		ActionObjects actionobjects = new ActionObjects();
		String dAdd1 = testData.get("Add1");
		String dAdd2 = testData.get("Add2");
		// String add3 = testData.get("add3");
		String dCity = testData.get("City");
		String dCounty = testData.get("County");
		String dState = testData.get("StateAbbreviation");
		String dPostalCode5 = testData.get("PostalCode5");
		dPostalCode5 = (String.valueOf(dPostalCode5).replaceFirst("\\.0+$", ""));
		String dPostalCode4 = testData.get("PostalCode4");
		dPostalCode4 = (String.valueOf(dPostalCode4).replaceFirst("\\.0+$", ""));

		String dHouse = testData.get("House");
		String dRoomAndBoard = testData.get("Room");
		String dNursingHome = testData.get("NursingHome");
		String dCenterDevDisabled = testData.get("CenterDevDisabled");
		String dApartment = testData.get("Apartment");
		String dGroupHome = testData.get("GroupHome");
		String dOther = testData.get("Other");
		String dOtherLivingInformation = testData.get("OtherLivingInformation");
		String dMailingAddress = testData.get("MailingAddress");
		String dInCareOf = testData.get("InCareOf");
		String dInCareOfName = testData.get("InCareOfName");

		try {

			dHouse = (String.valueOf(dHouse).replaceFirst("\\.0+$", ""));
			switch (dHouse) {
			case "0": // No
				break;
			case "1": // Yes
				click(sHouse, "House - rent/own/mortgage");
			}
			dRoomAndBoard = (String.valueOf(dRoomAndBoard).replaceFirst("\\.0+$", ""));
			switch (dRoomAndBoard) {
			case "0": // No
				break;
			case "1": // Yes
				click(sRoomAndBoard, "Room and board");
			}
			dNursingHome = (String.valueOf(dNursingHome).replaceFirst("\\.0+$", ""));
			switch (dNursingHome) {
			case "0": // No
				break;
			case "1": // Yes
				click(sNursingHome, "Nursing Home");
			}
			dCenterDevDisabled = (String.valueOf(dCenterDevDisabled).replaceFirst("\\.0+$", ""));
			switch (dCenterDevDisabled) {
			case "0": // No
				break;
			case "1": // Yes
				click(sCenterDevDisabled, "House - Center for Developmentally Disabled");
			}
			dApartment = (String.valueOf(dApartment).replaceFirst("\\.0+$", ""));
			switch (dApartment) {
			case "0": // No
				break;
			case "1": // Yes
				click(sApartment, "Apartment/duplex/triplex");
			}
			dGroupHome = (String.valueOf(dGroupHome).replaceFirst("\\.0+$", ""));
			switch (dGroupHome) {
			case "0": // No
				break;
			case "1": // Yes
				click(sGroupHome, "Group home/Foster care/Child careinstitution/Adult family home");
			}
			dOther = (String.valueOf(dOther).replaceFirst("\\.0+$", ""));
			switch (dOther) {
			case "0": // No
				break;
			case "1": // Yes
				click(sOther, "Other");
				type(sOtherLivingInformation, dOtherLivingInformation, "Other Living Information");
			}
			
			switch (dAdd1) {
			case "null":
				break;
			case "":
				break;
			case "0":
				break;
			default:
				type(sAdd1, dAdd1, "Street address");
			}

			switch (dAdd2) {
			case "null":
				break;
			case "":
				break;
			case "0":
				break;
			default:
				type(sAdd2, dAdd2, "Street Address 2");
			}

			switch (dCity) {
			case "null":
				break;
			case "":
				break;
			case "0":
				break;
			default:
				type(sCity, dCity, "City");
			}

			switch (dState) {
			case "null":
				break;
			case "":
				break;
			case "0":
				break;
			default:
				selectByValue(sState, dState, "State");
			}

			switch (dCounty) {
			case "null":
				break;
			case "":
				break;
			case "0":
				break;
			default:
				selectByValue(sCounty, dCounty, "County");
			}

			switch (dPostalCode5) {
			case "null":
				break;
			case "":
				break;
			case "0":
				break;
			default:
				type(sPostalCode5, dPostalCode5, "PostalCode5");
			}

			switch (dPostalCode5) {
			case "null":
				break;
			case "":
				break;
			case "0":
				break;
			default:
				type(sPostalCode4, dPostalCode4, "PostalCode4");
			}

			dInCareOf = (String.valueOf(dInCareOf).replaceFirst("\\.0+$", ""));
			switch (dInCareOf) {
			case "0": // No
				click(sInCareOfNo, "In Care Of No");
				break;
			case "1": // Yes
				click(sInCareOfYes, "In Care Of Yes");
				type(sInCareOfName, dInCareOfName, "In Care Of");
			}
			
			dMailingAddress = (String.valueOf(dMailingAddress).replaceFirst("\\.0+$", ""));
			switch (dMailingAddress) {
			case "0": // No
				click(sMailingAddressNo, "Is this also your mailing address - No?");
				break;
			case "1": // Yes
				click(sMailingAddressYes, "Is this also your mailing address - Yes?");
			}
						
		} catch (Exception e) {
			e.printStackTrace();
		}

		actionobjects.bSaveAndContinue();
		System.out.println("End of Page or Success - Household Address Page - Tell us about your address What's the household's living arrangement?");

	}

}