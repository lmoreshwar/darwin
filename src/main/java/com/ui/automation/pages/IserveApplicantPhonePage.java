package com.ui.automation.pages;

import java.util.HashMap;

import org.openqa.selenium.By;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;

/**
 * @author Douglas
 * @table ISERVE.?
 * @spreadsheet IServe_TestData.xlsx | Framework
 */
public class IserveApplicantPhonePage extends ActionEngine {

	private By sPhoneNumberYes = By.xpath("//*[@name='isAdditionalNumbers' and @value='Yes']/following-sibling::label");
	private By sPhoneNumberNo = By.xpath("//*[@name='isAdditionalNumbers' and @value='No']/following-sibling::label");
	private By sPhoneNumber = By.name("phoneNumber");
	private By sExt = By.name("ext");
	private By sPhoneType = By.name("type");
	private By sPhoneNumber0 = By.name("phoneNumber-0");
	private By sExt0 = By.name("ext-0");
	private By sPhoneType0 = By.name("type-0");
	private By sPhoneNumber1 = By.name("phoneNumber-1");
	private By sExt1 = By.name("ext-1");
	private By sPhoneType1 = By.name("type-1");
	private By sPhoneNumber2 = By.name("phoneNumber-2");
	private By sExt2 = By.name("ext-2");
	private By sPhoneType2 = By.name("type-2");
	private By sPhoneNumber3 = By.name("phoneNumber-3");
	private By sExt3 = By.name("ext-3");
	private By sPhoneType3 = By.name("type-3");
	private By sPhoneNumberOwner = By.name("phoneNumberOwner");
	private By sAllowVoicemailYes = By.xpath("//*[@name='allowVoicemail' and @value='Yes']/following-sibling::label");
	private By sAllowVoicemailNo = By.xpath("//*[@name='allowVoicemail' and @value='No']/following-sibling::label");
	private By sDontHavePhoneNumber = By.xpath("//*[@name='dontHavePhoneNumer' and @value='no-number']/following-sibling::label");
	

	public void setData(HashMap<String, String> testData) throws Throwable {
		
		ActionObjects actionobjects = new ActionObjects();
		String dAddPhoneNumber = testData.get("AddPhoneNumber");
		String dPhoneNumber = testData.get("PhoneNumber");
		String dExt = testData.get("Ext");
		String dPhoneType = testData.get("PhoneType");
		
		String dPhoneNumber0 = testData.get("PhoneNumber0");
		String dExt0 = testData.get("Ext0");
		String dPhoneType0 = testData.get("PhoneType0");
		
		String dPhoneNumber1 = testData.get("PhoneNumber1");
		String dExt1 = testData.get("Ext1");
		String dPhoneType1 = testData.get("PhoneType1");
		
		String dPhoneNumber2 = testData.get("PhoneNumber2");
		String dExt2 = testData.get("Ext2");
		String dPhoneType2 = testData.get("PhoneType2");
		
		String dPhoneNumberOwner = testData.get("PhoneNumberOwner");
		String dAllowVoicemail = testData.get("AllowVoicemail");
		String dDontHavePhoneNumber = testData.get("DontHavePhoneNumber");
		
		type(sPhoneNumber, dPhoneNumber, "PhoneNumber");
		type(sExt, dExt, "Ext");
		selectByValue(sPhoneType, dPhoneType, "Type");
		
		try {
		
			dAddPhoneNumber = (String.valueOf(dAddPhoneNumber).replaceFirst("\\.0+$", ""));
			switch (dAddPhoneNumber) {
			case "0": // No
				click(sPhoneNumberNo, "Phone Number No");
				break;
			case "1": // Yes
				click(sPhoneNumberYes, "Phone Number Yes");
				
				type(sPhoneNumber0, dPhoneNumber0, "PhoneNumber0");
				type(sExt0, dExt0, "Ext0");
				selectByValue(sPhoneType0, dPhoneType0, "Type0");
			
				type(sPhoneNumber1, dPhoneNumber1, "PhoneNumber1");
				type(sExt1, dExt1, "Ext`");
				selectByValue(sPhoneType1, dPhoneType1, "Type1");
				
				type(sPhoneNumber2, dPhoneNumber2, "PhoneNumber2");
				type(sExt2, dExt2, "Ext2");
				selectByValue(sPhoneType2, dPhoneType2, "Type2");
				
			}
			
			type(sPhoneNumberOwner, dPhoneNumberOwner, "sPhoneNumberOwner");
			
			dAllowVoicemail = (String.valueOf(dAllowVoicemail).replaceFirst("\\.0+$", ""));
			switch (dAllowVoicemail) {
			case "0": // No
				click(sAllowVoicemailNo, "AllowVoicemail No");
				break;
			case "1": // Yes
				click(sAllowVoicemailYes, "AllowVoicemail Yes");
			}
			
			dDontHavePhoneNumber = (String.valueOf(dDontHavePhoneNumber).replaceFirst("\\.0+$", ""));
			switch (dDontHavePhoneNumber) {
			case "0": // No
				break;
			case "1": // Yes
				click(sDontHavePhoneNumber, "DontHavePhoneNumber Yes");
			}
				
		} catch (Exception e) {
			e.printStackTrace();
		}

		actionobjects.bSaveAndContinue();
		System.out.println("End of Page or Success - Applicant Name Page - To start, please tell us about yourself Legally as it appears on your Social Security Card.");
	}
}