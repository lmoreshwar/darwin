package com.ui.automation.pages;

import java.util.HashMap;

import org.openqa.selenium.By;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;

/**
 * @author Douglas
 * @table ISERVE.?
 * @spreadsheet IServe_TestData.xlsx
 */
public class IserveApplicantNameWhereToReachYouPage extends ActionEngine {

	private By sPhoneNumber = By.name("phoneNumber");
	private By sEmailAddress = By.name("emailAddress");

	public void setData(HashMap<String, String> testData) throws Throwable {

		ActionObjects actionobjects = new ActionObjects();
		String dPhoneNumber = testData.get("PhoneNumber");
		String dEmailAddress = testData.get("EmailAddress");


		try {

			switch (dPhoneNumber) {
			case "null":
				break;
			case "":
				break;
			case "0":
				break;
			default:
				type(sPhoneNumber, dPhoneNumber, "Phone Number");
			}

			switch (dEmailAddress) {
			case "null":
				break;
			case "":
				break;
			case "0":
				break;
			default:
				type(sEmailAddress, dEmailAddress, "Email Address");
			}


		} catch (Exception e) {
			e.printStackTrace();
		}

		actionobjects.bSaveAndContinue();
		System.out.println("End of Page or Success - Applicant Where to Reach You Page - Tell us where to reach you.");
	}
}