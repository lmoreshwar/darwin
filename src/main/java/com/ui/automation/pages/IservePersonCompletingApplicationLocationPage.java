package com.ui.automation.pages;

import java.util.HashMap;

import org.openqa.selenium.By;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;

/**
 * @author Douglas
 * @table ISERVE.?
 * @spreadsheet IServe_TestData.xlsx
 */
public class IservePersonCompletingApplicationLocationPage extends ActionEngine {
	ActionObjects actionobjects = new ActionObjects();
	private By sApplicantRoleLocation1 = By.cssSelector(".usa-radio:nth-child(1) > .usa-radio__label");// TheComputerAtYourLocalDHHSOffice
	private By sApplicantRoleLocation2 = By.cssSelector(".usa-radio:nth-child(2) > .usa-radio__label");// ComputerAtHomeAtAFamilyMemberOrFriendHouseOrAtWork
	private By sApplicantRoleLocation3 = By.cssSelector(".usa-radio:nth-child(3) > .usa-radio__label");// CommunityAgency
	private By sApplicantRoleLocation4 = By.cssSelector(".usa-radio:nth-child(4) > .usa-radio__label");// Library
	private By sApplicantRoleLocation5 = By.cssSelector(".usa-radio:nth-child(5) > .usa-radio__label");// FoodBankOrFoodPantry
	private By sApplicantRoleLocation6 = By.cssSelector(".usa-radio:nth-child(6) > .usa-radio__label");// GoodNeighborCenter
	private By sApplicantRoleLocation7 = By.cssSelector(".usa-radio:nth-child(7) > .usa-radio__label");// Hospital
	private By sApplicantRoleLocation8 = By.cssSelector(".usa-radio:nth-child(8) > .usa-radio__label");// HealthcareCenter
	private By sApplicantRoleLocation9 = By.cssSelector(".usa-radio:nth-child(9) > .usa-radio__label");// PublicHealthAgency
	private By sApplicantRoleLocation10 = By.cssSelector(".usa-radio:nth-child(10) > .usa-radio__label");// OneWorldCommunityHealthCenter
	private By sApplicantRoleLocation11 = By.cssSelector(".usa-radio:nth-child(11) > .usa-radio__label");// ThePoncaChildrenMedicalOutreachProgram
	private By sApplicantRoleLocation12 = By.cssSelector(".usa-radio:nth-child(12) > .usa-radio__label");// ShelterHomeless
	private By sApplicantRoleLocation13 = By.cssSelector(".usa-radio:nth-child(13) > .usa-radio__label");// NoneOfTheAbove

	public void setData(HashMap<String, String> testData) throws Throwable {

		String dApplicantRoleLocation1 = testData.get("ApplicantRoleLocation1");//
		String dApplicantRoleLocation2 = testData.get("ApplicantRoleLocation2");//
		String dApplicantRoleLocation3 = testData.get("ApplicantRoleLocation3");//
		String dApplicantRoleLocation4 = testData.get("ApplicantRoleLocation4");//
		String dApplicantRoleLocation5 = testData.get("ApplicantRoleLocation5");//
		String dApplicantRoleLocation6 = testData.get("ApplicantRoleLocation6");//
		String dApplicantRoleLocation7 = testData.get("ApplicantRoleLocation7");//
		String dApplicantRoleLocation8 = testData.get("ApplicantRoleLocation8");//
		String dApplicantRoleLocation9 = testData.get("ApplicantRoleLocation9");//
		String dApplicantRoleLocation10 = testData.get("ApplicantRoleLocation10");//
		String dApplicantRoleLocation11 = testData.get("ApplicantRoleLocation11");//
		String dApplicantRoleLocation12 = testData.get("ApplicantRoleLocation12");//
		String dApplicantRoleLocation13 = testData.get("ApplicantRoleLocation13");//

		try {

			dApplicantRoleLocation1 = (String.valueOf(dApplicantRoleLocation1).replaceFirst("\\.0+$", ""));
			switch (dApplicantRoleLocation1) {
			case "0": // No/
				break;
			case "1": // Yes/
				click(sApplicantRoleLocation1, " - I am the Applicant.");
			}

			dApplicantRoleLocation2 = (String.valueOf(dApplicantRoleLocation2).replaceFirst("\\.0+$", ""));
			switch (dApplicantRoleLocation2) {
			case "0": // No/
				break;
			case "1": // Yes/
				click(sApplicantRoleLocation2, " - I am an authorized representative.");
			}
			dApplicantRoleLocation3 = (String.valueOf(dApplicantRoleLocation3).replaceFirst("\\.0+$", ""));
			switch (dApplicantRoleLocation3) {
			case "0": // No/
				break;
			case "1": // Yes/
				click(sApplicantRoleLocation3, " - I am a parent or legal guardian of the minor.");
			}

			dApplicantRoleLocation4 = (String.valueOf(dApplicantRoleLocation4).replaceFirst("\\.0+$", ""));
			switch (dApplicantRoleLocation4) {
			case "0": // No/
				break;
			case "1": // Yes/
				click(sApplicantRoleLocation4,
						" - I am a guardian, conservator, or power of attorney acting on behalf of the Applicant.");
			}

			dApplicantRoleLocation5 = (String.valueOf(dApplicantRoleLocation5).replaceFirst("\\.0+$", ""));
			switch (dApplicantRoleLocation5) {
			case "0": // No/
				break;
			case "1": // Yes/
				click(sApplicantRoleLocation5, " - I am a member of the household listed on the application.");
			}

			dApplicantRoleLocation6 = (String.valueOf(dApplicantRoleLocation6).replaceFirst("\\.0+$", ""));
			switch (dApplicantRoleLocation6) {
			case "0": // No/
				break;
			case "1": // Yes/
				click(sApplicantRoleLocation6, " - I am a contracted Medicaid provider.");
			}

			dApplicantRoleLocation7 = (String.valueOf(dApplicantRoleLocation7).replaceFirst("\\.0+$", ""));
			switch (dApplicantRoleLocation6) {
			case "0": // No/
				break;
			case "1": // Yes/
				click(sApplicantRoleLocation7, " - I am a DHHS worker.");
			}

			dApplicantRoleLocation8 = (String.valueOf(dApplicantRoleLocation8).replaceFirst("\\.0+$", ""));
			switch (dApplicantRoleLocation8) {
			case "0": // No/
				break;
			case "1": // Yes/
				click(sApplicantRoleLocation8, " - I am a DHHS worker.");
			}

			dApplicantRoleLocation9 = (String.valueOf(dApplicantRoleLocation9).replaceFirst("\\.0+$", ""));
			switch (dApplicantRoleLocation9) {
			case "0": // No/
				break;
			case "1": // Yes/
				click(sApplicantRoleLocation9, " - I am a DHHS worker.");
			}

			dApplicantRoleLocation10 = (String.valueOf(dApplicantRoleLocation10).replaceFirst("\\.0+$", ""));
			switch (dApplicantRoleLocation10) {
			case "0": // No/
				break;
			case "1": // Yes/
				click(sApplicantRoleLocation10, " - I am a DHHS worker.");
			}

			dApplicantRoleLocation11 = (String.valueOf(dApplicantRoleLocation11).replaceFirst("\\.0+$", ""));
			switch (dApplicantRoleLocation11) {
			case "0": // No/
				break;
			case "1": // Yes/
				click(sApplicantRoleLocation11, " - I am a DHHS worker.");
			}

			dApplicantRoleLocation12 = (String.valueOf(dApplicantRoleLocation12).replaceFirst("\\.0+$", ""));
			switch (dApplicantRoleLocation12) {
			case "0": // No/
				break;
			case "1": // Yes/
				click(sApplicantRoleLocation12, " - I am a DHHS worker.");
			}

			dApplicantRoleLocation13 = (String.valueOf(dApplicantRoleLocation13).replaceFirst("\\.0+$", ""));
			switch (dApplicantRoleLocation13) {
			case "0": // No/
				break;
			case "1": // Yes/
				click(sApplicantRoleLocation13, " - I am a DHHS worker.");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		actionobjects.bSaveAndContinue();
		System.out.println(
				"End of Page or Success - Person Completing Application Location Page - Document Upload Application Submission End.");

	}

}