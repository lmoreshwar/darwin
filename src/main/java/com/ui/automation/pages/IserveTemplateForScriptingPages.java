package com.ui.automation.pages;

import java.util.HashMap;

import org.openqa.selenium.By;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;

/**
 * @author Douglas
 * @table ISERVE.
 *        <table>
 * @variable s = screen, d = data
 * @description This is a template script meant to support initial page development It contains variable formatting,
 *              structure and elements
 */
public class IserveTemplateForScriptingPages extends ActionEngine {
	ActionObjects actionobjects = new ActionObjects();
	final By sMedicaid = By.cssSelector(".usa-form-group:nth-child(1) > .usa-checkbox__label");
	final By sFirstName = By.name("firstName");
	private By sStableAddress = By.cssSelector(".usa-form-group:nth-child(1) > .usa-radio__label");
	private By sAdd1 = By.id("streetaddress.home1");

	public void setDBData(HashMap<String, String> testData) throws Throwable {

		String dMedicaid = testData.get("Medicaid");
		String dAdd1 = testData.get("add1");
		String dFirstName = testData.get("FirstName");
		String dStableAddress = testData.get("StableAddress");
		String dPostalCode4 = testData.get("PostalCode4");
		dPostalCode4 = (String.valueOf(dPostalCode4).replaceFirst("\\.0+$", "")); // Removes .0 from inbound data

		try {

			switch (dAdd1) {
			case "null":
				break;
			case "":
				break;
			case "0":
				break;
			default:
				type(sAdd1, dAdd1, "Street Address");
			}

			switch (dMedicaid) {
			case "0": // No
				break;
			case "1": // Yes
				click(sMedicaid, "Medicaid");
			}

			type(sFirstName, dFirstName, "First Name");

			switch (dStableAddress) {
			case "0": // No
				break;
			case "1": // Yes
				click(sStableAddress, "Stable address - I  have a stable place to live");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		actionobjects.bSaveAndContinue();
		System.out.println("End of Page or Success - Program Selection Page.");
	}
}