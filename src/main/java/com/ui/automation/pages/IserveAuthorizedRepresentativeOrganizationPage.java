package com.ui.automation.pages;

import java.util.HashMap;

import org.openqa.selenium.By;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;

/**
 * @author Douglas
 * @table ISERVE.?
 * @spreadsheet IServe_TestData.xlsx
 */
public class IserveAuthorizedRepresentativeOrganizationPage extends ActionEngine {

	ActionObjects actionobjects = new ActionObjects();
	private By sPartOfOrganizationYes = By.xpath("//label[contains(text(),'Yes')]");
	private By sPartOfOrganizationNo = By.xpath("//label[contains(text(),'No')]");
	private By sPartOfOrganizationName = By.name("organizationName");
	private By sPartOfOrganizationId = By.name("organizationId");


	public void setData(HashMap<String, String> testData) throws Throwable {

		String dPartOfOrganization = testData.get("PartOfOrganization");
		String dPartOfOrganizationName = testData.get("PartOfOrganizationName");
		String dPartOfOrganizationId = testData.get("PartOfOrganizationId");

		try {

			dPartOfOrganization = (String.valueOf(dPartOfOrganization).replaceFirst("\\.0+$", ""));
			switch (dPartOfOrganization) {
			case "0": // No
				click(sPartOfOrganizationNo, "PartOfOrganization No");
				break;
			case "1": // Yes
				click(sPartOfOrganizationYes, "PartOfOrganization Yes");
				type(sPartOfOrganizationName, dPartOfOrganizationName, "dPartOfOrganizationName");
				type(sPartOfOrganizationId, dPartOfOrganizationId, "dPartOfOrganizationId");

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		actionobjects.bSaveAndContinue();
		System.out.println("End of Page or Success - Authorized Representative Organization Page");
	}

}