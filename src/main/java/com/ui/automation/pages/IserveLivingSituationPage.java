package com.ui.automation.pages;

import java.util.HashMap;

import org.openqa.selenium.By;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;

/**
 * @author Douglas
 * @table ISERVE.?
 * @spreadsheet IServe_TestData.xlsx
 */
public class IserveLivingSituationPage extends ActionEngine {

	private By sStableAddress = By.cssSelector(".usa-form-group:nth-child(1) > .usa-radio__label");
	private By sTemporaryAddress = By.cssSelector(".usa-form-group:nth-child(2) > .usa-radio__label");
	private By sHomeless = By.cssSelector(".usa-form-group:nth-child(3) > .usa-radio__label");

	public void setData(HashMap<String, String> testData) throws Throwable {
		ActionObjects actionobjects = new ActionObjects();
		String dStableAddress = testData.get("StableAddress");
		String dTemporaryAddress = testData.get("TemporaryAddress");
		String dHomeless = testData.get("Homeless");

		try {

			dStableAddress = (String.valueOf(dStableAddress).replaceFirst("\\.0+$", ""));
			switch (dStableAddress) {
			case "0": // No
				break;
			case "1": // Yes
				click(sStableAddress, "Stable address - I  have a stable place to live");
			}

			dTemporaryAddress = (String.valueOf(dTemporaryAddress).replaceFirst("\\.0+$", ""));
			switch (dTemporaryAddress) {
			case "0": // No
				break;
			case "1": // Yes
				click(sTemporaryAddress, "Temporary address - I have a place or places to live, but they aren't permanent");
			}

			dHomeless = (String.valueOf(dHomeless).replaceFirst("\\.0+$", ""));
			switch (dHomeless) {
			case "0": // No
				break;
			case "1": // Yes
				click(sHomeless, "Homeless - I don't currently have an address");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		actionobjects.bSaveAndContinue();
		System.out.println(
				"End of Page or Success - Living Situation - Tell us about your living situation. What's your current living situation?.");
	}

}