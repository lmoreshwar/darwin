package com.ui.automation.utilities;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.text.ParseException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;

public class StringUtilities {

	
	public static boolean isDigitInString(String input){
		Pattern p = Pattern.compile("([0-9])");
		Matcher m = p.matcher(input);
		if(m.find()){
			System.out.println(input+" has digit "+m.find());
			return true;
		}else{
			return false; 
		}
	}

	
	/**
	 * deleteDirectory, Delete directory from local machine
	 *
	 * @param directoryPath of (String),  path for the directory to delete
	 */
	public void deleteDirectory(String directoryPath) throws IOException {
		FileUtils.deleteDirectory(new File(directoryPath));
	}

	/**
	 * getRandomString, Get random String
	 *
	 * @param noOfCharacters of (int), Number of characters to get randomly
	 * @return String
	 */
	public static String getRandomString(int noOfCharacters) throws IOException {
		return RandomStringUtils.randomAlphabetic(noOfCharacters);
	}

	/**
	 * getRandomNumeric, Get random Numeric
	 *
	 * @param noOfCharacters of (int),  Number of characters to get randomly
	 * @return String
	 */
	public static String getRandomNumeric(int noOfCharacters) throws IOException {
		return RandomStringUtils.randomNumeric(noOfCharacters);
	}
	
	/**
	 * subString, Function to get sub string of given actual string text
	 *
	 * @param text       of (String), Actual text
	 * @param startIndex of (int), Start index of sub string
	 * @param endIndex   of (int), end index of sub string
	 * @return : String
	 */
	protected String subString(String text, int startIndex, int endIndex) {
		String flag = null;
		try {
			flag = text.substring(startIndex, endIndex);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return flag;
	}
	
	/**
	 * replaceAll, Function to replace the regular expression values with client required values
	 *
	 * @param text        of (String)
	 * @param pattern     of (String), regular expression of actual value
	 * @param replaceWith of (String), value to replace the actual
	 * @return : String
	 */
	public String replaceAll(String text, String pattern, String replaceWith) {
		String flag = null;
		try {
			flag = text.replaceAll(pattern, replaceWith);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return flag;
	}
	
	/**
	  * param :: Hashtable with string inputs
	  * return ::String
	  * throws :: throwable
	  * methodName :: generateRandomNumber
	  * description :: To select ---
	  */
	public static String generateRandomNumber(int length) {
		String randomNumber = "1";
		int retryCount = 1;
		while (retryCount > 0) {
			String number = Double.toString(Math.random());
			number = number.replace(".", "");
			if (number.length() > length) {
				randomNumber = number.substring(0, length);
			} else {
				int remainingLength = length - number.length() + 1;
				randomNumber = generateRandomNumber(remainingLength);
			}
			if (randomNumber.length() < length) {
				retryCount++;
			} else {
				retryCount = 0;
			}
		}
		return randomNumber;
	}
	/**
	 * generateRandomNumber
	 *description :: This is used to generate random number with 8 digit as current Year(YY)and system time
	 * @return int
	 * @throws Throwable the throwable
	 */
	public long generateRandomNumber() throws Throwable {
		/*Random generator = new Random();
		return generator.nextInt(9999) + 10000;*/
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyhhmmss");
		String currentdate = sdf.format(date);
		long ran = Long.parseLong(currentdate);
		return ran;

	}

	/**
	 * generateRandomNumber
	 *description :: This is used to generate random number with 8 digit as current Year(YY)and system time
	 * @return int
	 * @throws Throwable the throwable
	 */
	public static String generateRandomNumberBasedOnLength(int length) throws Throwable {
		/*Random generator = new Random();
		return generator.nextInt(9999) + 10000;*/
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyHHmmssSdSMSs");
		String currentdate = sdf.format(date);
		return new StringBuffer(currentdate).reverse().substring(0,length);
	}
	
	
	public static int generateRandomNumber(int minSize, int maxSize)
	{
		Random rand = new Random();
		if(maxSize > minSize)
		{
		int randomNum =minSize + rand.nextInt((maxSize - minSize));
		System.out.println(randomNum);
		return randomNum;
		}
		else
		{
			int randomNum =minSize + rand.nextInt((minSize - maxSize)+1);
			System.out.println(randomNum);
			return randomNum;
		}
	}
	
	public static boolean isValidDate(String inDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(inDate.trim());
        } catch (ParseException pe) {
            return false;
        }
        return true;
    }
	
	public static boolean isValidPhoneNumber(String phoneNo){
        return phoneNo.matches("[1-9]\\d{2}-\\d{3}-\\d{4}");
	}
	
	public static boolean isAlpha(String name) {
	    return name.matches("[a-zA-Z]+");
	}
	
	public static boolean isValidZip(String phoneNo){
        return phoneNo.matches("\\d{5}");
	}

public static int generateNumberPerRequirment(String requirment, int maxNumber)
{
	switch (requirment) {
	case "lower":
		return generateRandomNumber(1, maxNumber);
	case "greater":
		return generateRandomNumber(maxNumber, maxNumber+100);
	default:
		return maxNumber;
	}
}
/**
  * param :: String
  * return ::String
  * throws :: throwable
  * methodName :: splitAndTrimTheString
  * description :: To remove split and trim the string
  */
public static String splitTrimAndAppendTheString(String value, String splitCharacter)
{
	return value.split(Pattern.quote(splitCharacter))[0].trim()+splitCharacter+value.split(Pattern.quote(splitCharacter))[1].trim();
}
}
