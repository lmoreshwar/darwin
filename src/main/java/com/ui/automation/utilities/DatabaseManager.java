package com.ui.automation.utilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

/**
 * @author Eyasu
 *
 */
public class DatabaseManager {
	// Connection object
	private Connection con;
	// Statement object
	private static Statement stmt;
	// Data base class
	private String DB_CLASS = ConfigManager.getProperty("DBDriver").trim();
	// Constant for Database URL
	private String DB_URL = ConfigManager.getProperty("URLAzure").trim();
	// Constant for Database username
	private String DB_USER = ConfigManager.getProperty("UserAzure").trim();
	// Constant for Database Password
	private String DB_PASSWORD = ConfigManager.getProperty("PasswordAzure").trim();
	// Get the contents of table from the Data Base
	private static ResultSet rs;

	/**
	 * @throws Exception
	 */
	public void setUp() throws Throwable {
		try {
			System.out.println("DataBaseManager");
			// Make the database connection
			Class.forName(DB_CLASS);
			// Get connection to DB
			con = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
			// Statement object to send the SQL statement to the Database
			stmt = con.createStatement();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param query
	 * @return
	 * @throws Exception
	 */
	public HashMap<String, String> getData(String query) throws Throwable {
		HashMap<String, String> testData = null;
		try {
			setUp();
			rs = stmt.executeQuery(query);
			testData = convertResultSetToHashMap(rs);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			tearDown();
		}
		return testData;
	}

	/**
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	public HashMap<String, String> convertResultSetToHashMap(ResultSet rs) throws Throwable {
		HashMap<String, String> testData = null;
		ResultSetMetaData md = rs.getMetaData();
		int columns = md.getColumnCount();
		while (rs.next()) {
			testData = new HashMap<>(columns);
			for (int i = 1; i <= columns; ++i) {
				testData.put(md.getColumnName(i), rs.getString(md.getColumnName(i)));
			}
		}
		return testData;
	}

	/**
	 * @throws Exception
	 */
	public void tearDown() throws Throwable {
		// Close DB result set
		if (rs != null) {
			rs.close();
		}

		// Close DB statement
		if (stmt != null) {
			stmt.close();
		}

		// Close DB connection
		if (con != null) {
			con.close();
		}
	}
}
