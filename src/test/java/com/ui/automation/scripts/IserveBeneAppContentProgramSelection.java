package com.ui.automation.scripts;

import static org.testng.Assert.assertEquals;

import java.util.HashMap;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;
import com.ui.automation.pages.HomePage;
import com.ui.automation.pages.IserveProgramSelectionPage;
import com.ui.automation.utilities.ExcelLib;

/**
 * @author Douglas
 *
 */
public class IserveBeneAppContentProgramSelection extends ActionEngine {
	ExcelLib excelLib = new ExcelLib();
	HomePage homepage = new HomePage();
	ActionObjects actionobjects = new ActionObjects();
	IserveProgramSelectionPage IserveProgramSelectionPage = new IserveProgramSelectionPage();
	private By sMedicaid = By.cssSelector(".usa-form-group:nth-child(1) > .usa-checkbox__label");
	private By sSSAD = By.cssSelector(".usa-form-group:nth-child(11) > .usa-checkbox__label");

//  /**
//   * Testing Question Content using Excel for Test Data
//   *
//   * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
//   */
	@Test(priority = 0)
	public void iServeBeneAppContentProgramSelection() throws Throwable {
		try {
			HashMap<String, String> testData = excelLib.getData("ISERVE", "ProgramSelectionContent");
			reportLogger = extent.startTest("IserveBeneAppContentProgramSelection", "ISERVE Content Excel");
			homepage.noLoginCredentialsStartBenefitsApplication();
			//IserveProgramSelectionPage.setData(testData);

			By sPageTitle = By.xpath("//h1[normalize-space()='Program Selection']");
			String sPageTitleContent = getText(sPageTitle, "PageTitle Content");
			String dPageTitle = testData.get("PageTitle");
			By sPageQuestion = By.xpath("//legend[@class='usa-legend']");
			String sPageQuestionContent = getText(sPageQuestion, "PageQuestion Content");
			String dPageQuestion = testData.get("PageQuestion");

			By sMedicaidContent = By.xpath("//label[@for='program-checkbox_1']");
			String sMedicaidContent1 = getText(sMedicaidContent, "Medicaid Content");
			String dMedicaidContent = testData.get("MedicaidContent");
			click(sMedicaid, "Medicaid");

			By sHCBSContent = By.xpath("//label[@for='program-checkbox_2']");
			String sHCBSContent1 = getText(sHCBSContent, "HCBS Content");
			String dHCBSContent = testData.get("HCBSContent");

			By sSNAPContent = By.xpath("//label[@for='program-checkbox_3']");
			String sSNAPContent1 = getText(sSNAPContent, "SNAP Content");
			String dSNAPContent = testData.get("SNAPContent");

			By sLIHEAPContent = By.xpath("//label[@for='program-checkbox_4']");
			String sLIHEAPContent1 = getText(sLIHEAPContent, "LIHEAP Content");
			String dLIHEAPContent = testData.get("LIHEAPContent");

			By sRRPContent = By.xpath("//label[@for='program-checkbox_5']");
			String sRRPContent1 = getText(sRRPContent, "RRP Content");
			String dRRPContent = testData.get("RRPContent");

			By sADCContent = By.xpath("//label[@for='program-checkbox_6']");
			String sADCContent1 = getText(sADCContent, "ADC Content");
			String dADCContent = testData.get("ADCContent");

			By sCCSContent = By.xpath("//label[@for='program-checkbox_7']");
			String sCCSContent1 = getText(sCCSContent, "CCS Content");
			String dCCSContent = testData.get("CCSContent");

			By sEMAContent = By.xpath("//label[@for='program-checkbox_8']");
			String sEMAContent1 = getText(sEMAContent, "EMA Content");
			String dEMAContent = testData.get("EMAContent");

			By sAABDContent = By.xpath("//label[@for='program-checkbox_9']");
			String sAABDContent1 = getText(sAABDContent, "AABD Content");
			String dAABDContent = testData.get("AABDContent");

			By sSDPContent = By.xpath("//label[@for='program-checkbox_10']");
			String sSDPContent1 = getText(sSDPContent, "SDP Content");
			String dSDPContent = testData.get("SDPContent");

			By sSSADContent = By.xpath("//label[@for='program-checkbox_11']");
			String sSSADContent1 = getText(sSSADContent, "SSAD Content");
			String dSSADContent = testData.get("SSADContent");

			click(sSSAD, "sSSAD");

				By sSSAD1Content = By.xpath("//label[@for='ssad-service-checkbox_1']");
				String sSSAD1Content1 = getText(sSSAD1Content, "SSAD1 Content");
				String dSSAD1Content = testData.get("SSAD1Content");

				By sSSAD2Content = By.xpath("//label[@for='ssad-service-checkbox_2']");
				String sSSAD2Content1 = getText(sSSAD2Content, "SSAD2 Content");
				String dSSAD2Content = testData.get("SSAD2Content");

				By sSSAD3Content = By.xpath("//label[@for='ssad-service-checkbox_3']");
				String sSSAD3Content1 = getText(sSSAD3Content, "SSAD3 Content");
				String dSSAD3Content = testData.get("SSAD3Content");

				By sSSAD4Content = By.xpath("//label[@for='ssad-service-checkbox_4']");
				String sSSAD4Content1 = getText(sSSAD4Content, "SSAD4 Content");
				String dSSAD4Content = testData.get("SSAD4Content");

				By sSSAD5Content = By.xpath("//label[@for='ssad-service-checkbox_5']");
				String sSSAD5Content1 = getText(sSSAD5Content, "SSAD5 Content");
				String dSSAD5Content = testData.get("SSAD5Content");

				By sSSAD6Content = By.xpath("//label[@for='ssad-service-checkbox_6']");
				String sSSAD6Content1 = getText(sSSAD6Content, "SSAD6 Content");
				String dSSAD6Content = testData.get("SSAD6Content");

			By sPASContent = By.xpath("//label[@for='program-checkbox_12']");
			String sPASContent1 = getText(sPASContent, "PAS Content");
			String dPASContent = testData.get("PASContent");

			assertEquals(sPageTitleContent, dPageTitle, "Page Title DOES NOT Match");
			assertEquals(sPageQuestionContent, dPageQuestion, "PAS Program Content DOES NOT Match");
			assertEquals(sMedicaidContent1, dMedicaidContent, "Medicaid Program Content DOES NOT Match");
			assertEquals(sHCBSContent1, dHCBSContent, "HCBS Program Content DOES NOT Match");
			assertEquals(sSNAPContent1, dSNAPContent, "SNAP Program Content DOES NOT Match");
			assertEquals(sLIHEAPContent1, dLIHEAPContent, "HCBS Program Content DOES NOT Match");
			assertEquals(sRRPContent1, dRRPContent, "RRP Program Content DOES NOT Match");
			assertEquals(sADCContent1, dADCContent, "ADC Program Content DOES NOT Match");
			assertEquals(sCCSContent1, dCCSContent, "CCS Program Content DOES NOT Match");
			assertEquals(sEMAContent1, dEMAContent, "EMA Program Content DOES NOT Match");
			assertEquals(sAABDContent1, dAABDContent, "AABD Program Content DOES NOT Match");
			assertEquals(sSSADContent1, dSSADContent, "SSAD Program Content DOES NOT Match");
				assertEquals(sSSAD1Content1, dSSAD1Content, "SSAD Program Content DOES NOT Match");
				assertEquals(sSSAD2Content1, dSSAD2Content, "SSAD Program Content DOES NOT Match");
				assertEquals(sSSAD3Content1, dSSAD3Content, "SSAD Program Content DOES NOT Match");
				assertEquals(sSSAD4Content1, dSSAD4Content, "SSAD Program Content DOES NOT Match");
				assertEquals(sSSAD5Content1, dSSAD5Content, "SSAD Program Content DOES NOT Match");
				assertEquals(sSSAD6Content1, dSSAD6Content, "SSAD Program Content DOES NOT Match");
			assertEquals(sSDPContent1, dSDPContent, "SDP Program Content DOES NOT Match");
			assertEquals(sPASContent1, dPASContent, "PAS Program Content DOES NOT Match");

			IserveProgramSelectionPage.setData(testData);

		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("IserveBeneAppContentProgramSelection");
	}

}