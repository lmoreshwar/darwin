package com.ui.automation.scripts;

import java.util.HashMap;

import org.testng.annotations.Test;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;
import com.ui.automation.pages.HomePage;
import com.ui.automation.utilities.ExcelLib;

/**
 * @author Douglas
 *
 */
public class IserveRoutingLoginAuthentication extends ActionEngine {
	ExcelLib excelLib = new ExcelLib();
	HomePage homepage = new HomePage();
	ActionObjects actionobjects = new ActionObjects();

	@Test(priority = 0)
	public void iServeRoutingLoginAuthentication() throws Throwable {
		try {
			HashMap<String, String> testData = excelLib.getData("ISERVE", "HomePageLogin");
			reportLogger = extent.startTest("Verify Site Access and Login", "ISERVE URL and Login");
			homepage.noLoginCredentialsAccessURL();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("IserveRoutingLoginAuthentication");
	}

}