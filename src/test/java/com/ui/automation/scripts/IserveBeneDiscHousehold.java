package com.ui.automation.scripts;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;
import com.ui.automation.pages.HomePage;
import com.ui.automation.pages.IserveBenefitDiscoveryBenefitResultsPage;
import com.ui.automation.pages.IserveBenefitDiscoveryHouseHoldInfoPage;
import com.ui.automation.pages.IserveHouseholdApplicantInfoPage;
import com.ui.automation.utilities.ExcelLib;

/**
 * @author Eyasu
 *
 */
public class IserveBeneDiscHousehold extends ActionEngine {
	public static String path = System.getProperty("user.dir") + File.separator + "TestData" + File.separator+ "iServe_Benefit_Discovery.xlsx";
	ExcelLib excelLib = new ExcelLib(path);
	HomePage homepage = new HomePage();
	ActionObjects actionobjects = new ActionObjects();
	SoftAssert softAssert = new SoftAssert();
	IserveHouseholdApplicantInfoPage IserveHouseholdApplicantInfoPage = new IserveHouseholdApplicantInfoPage();
	IserveBenefitDiscoveryHouseHoldInfoPage IserveBenefitDiscoveryHouseHoldInfoPage = new IserveBenefitDiscoveryHouseHoldInfoPage();
	IserveBenefitDiscoveryBenefitResultsPage IserveBenefitDiscoveryBenefitResultsPage = new IserveBenefitDiscoveryBenefitResultsPage();

	/**
	 * Testing Household Pages and Applicant Scenarios.
	 *
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	@Test(priority = 0)
	public void iServeBeneDiscHousehold() throws Throwable {
		try {
			List<HashMap<String, String>> testDataList = excelLib.getDataList("ISERVE", "BenefitDiscovery");
			//HashMap<String, String> testData = testDataList.get(0);
			for(HashMap<String, String> testData : testDataList) {
			reportLogger = extent.startTest(testData.get("Scenario"), "Benefit Discovery Household");
			homepage.noLoginCredentialsGoToBenefitDiscovery();
			actionobjects.bStart();
			IserveHouseholdApplicantInfoPage.setHouseHoledData(testData);
			actionobjects.bContinue();
			IserveBenefitDiscoveryHouseHoldInfoPage.setData(testData);
			IserveBenefitDiscoveryBenefitResultsPage.selectBenefitDiscoveryResults(testData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("******************** IserveBeneDiscHousehold ********************");
	}

}