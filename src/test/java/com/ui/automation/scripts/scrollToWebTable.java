package com.ui.automation.scripts;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;
import com.ui.automation.actions.TestDataPrefix;
import com.ui.automation.pages.HomePage;
import com.ui.automation.utilities.ExcelLib;
import com.ui.automation.utilities.StringUtilities;

import ch.qos.logback.core.recovery.ResilientSyslogOutputStream;

/**
 * @author Douglas
 *
 */
public class scrollToWebTable extends ActionEngine {
	ExcelLib excelLib = new ExcelLib();
	HomePage homepage = new HomePage();
	ActionObjects actionobjects = new ActionObjects();

	@Test(priority = 0)
	public void iServeRoutingLoginAuthentication() throws Throwable {
		try {
			reportLogger = extent.startTest("Verify Site Access and Login", "ISERVE URL and Login");

			HashMap<String,String> testData1 = excelLib.getData("Login", "MySheetTest1");



			List<HashMap<String, String>> testDataList =  excelLib.getDataList("TESTMORE", "MySheetTest1");
			HashMap<String, String> testData = testDataList.get(0);
			String count = testData.get("Count");
			System.out.println(count);
			count = (String.valueOf(count).replaceFirst("\\.0+$", ""));
			System.out.println(count);
			System.out.println(testData.get("Mandal"));
			click(By.id("land"), "Land");
			By district = By.id("districtCode");
			By mandal = By.id("mandalCode");
			By village = By.id("villageCode");
			By villageCount = By.xpath("//*[@name='villageCode']//option");
			isElementDisplayed(By.xpath("//h2[text()='Market Value Search']"), "Dispalyed");
			selectByVisibleText(district, testData.get("District"), "District");
			selectByVisibleText(mandal, testData.get("Mandal"), "Mandal");
			selectByVisibleText(village, testData.get("Village"),"Village");
			int count1 = getElementsSize(villageCount, webDriver);
			System.out.println(count1);
			click(By.name("submit"), "name");
			EventFiringWebDriver eventFiringWebDriver = new EventFiringWebDriver(webDriver);
			eventFiringWebDriver.executeScript("document.querySelector('.info_service').scrollTop=500");
			click(By.xpath("//*[@value='Back']"), "Back");


		} catch (Exception e) {
			e.printStackTrace();

		}
		System.out.println("IserveRoutingLoginAuthentication");
	}

}