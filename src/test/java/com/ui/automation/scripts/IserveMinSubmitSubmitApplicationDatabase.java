package com.ui.automation.scripts;

import java.util.HashMap;

import org.testng.annotations.Test;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;
import com.ui.automation.pages.HomePage;
import com.ui.automation.pages.IserveBenefitDiscoveryHouseHoldInfoPage;
import com.ui.automation.pages.IserveHouseholdAddressPage;
import com.ui.automation.pages.IserveLegalTermsPage;
import com.ui.automation.pages.IserveLivingSituationPage;
import com.ui.automation.pages.IserveNameTDMPage;
import com.ui.automation.pages.IserveProgramSelectPrivacyPage;
import com.ui.automation.pages.IserveProgramSelectionTDMPage;
import com.ui.automation.pages.IserveSignaturePage;
import com.ui.automation.utilities.DatabaseManager;

/**
 * @author Douglas
 *
 */
public class IserveMinSubmitSubmitApplicationDatabase extends ActionEngine {
	DatabaseManager databaseManager = new DatabaseManager();
	HomePage homepage = new HomePage();
	ActionObjects actionobjects = new ActionObjects();
	IserveProgramSelectionTDMPage IserveProgramSelectionTDMPage = new IserveProgramSelectionTDMPage();
	IserveNameTDMPage IserveNameTDMPage = new IserveNameTDMPage();
	IserveHouseholdAddressPage ISERVEAddressPage = new IserveHouseholdAddressPage();
	IserveSignaturePage IserveSignaturePage = new IserveSignaturePage();
	IserveBenefitDiscoveryHouseHoldInfoPage ISERVEHouseHoldPage = new IserveBenefitDiscoveryHouseHoldInfoPage();
	IserveLegalTermsPage IserveLegalTermsPage = new IserveLegalTermsPage();
	IserveLivingSituationPage IserveLivingSituationPage = new IserveLivingSituationPage();
	IserveProgramSelectPrivacyPage IserveProgramSelectPrivacyPage = new IserveProgramSelectPrivacyPage();

  /**
   * Testing Minimal Submit functionality using Test Database
   *
   * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
   */
	@Test(priority = 0)
	public void iServeMinSubmitSubmitApplicationDatabase() throws Throwable {
		String pullQuery = "select * from ISERVE.IserveTestData where FrameWorkID > '0'";
		try {
			HashMap<String, String> testData = databaseManager.getData(pullQuery);
			reportLogger = extent.startTest("Applicant Submits 'Minimal Submission' of an Application",	"ISERVE MINIMALSUBMIT Database");
			homepage.noLoginCredentialsStartBenefitsApplication();
			IserveProgramSelectionTDMPage.setDBData(testData);
			IserveProgramSelectPrivacyPage.setData(testData);
			actionobjects.bSaveAndContinue();
			IserveNameTDMPage.setDBData(testData);
			actionobjects.bSaveAndContinue();

		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("IserveMinSubmitSubmitApplicationDatabase");
	}

}